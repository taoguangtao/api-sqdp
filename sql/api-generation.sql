/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80011
 Source Host           : localhost:3306
 Source Schema         : api-generation

 Target Server Type    : MySQL
 Target Server Version : 80011
 File Encoding         : 65001

 Date: 02/09/2023 11:48:31
*/

DROP DATABASE IF EXISTS `api-generation`;

CREATE DATABASE  `api-generation` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin;

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

USE `api-generation`;

-- ----------------------------
-- Table structure for api_gen_access
-- ----------------------------
DROP TABLE IF EXISTS `api_gen_access`;
CREATE TABLE `api_gen_access`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `api_id` bigint(20) NOT NULL COMMENT 'api的id',
  `entering_ginseng` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL COMMENT '入参json格式类型',
  `exotic_ginseng` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL COMMENT '出参json格式类型',
  `report_name_format` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL COMMENT '报表名称格式',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `api_id_unique`(`api_id`) USING BTREE COMMENT 'api_id的出入参唯一'
) ENGINE = InnoDB AUTO_INCREMENT = 41 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = 'api出入参' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for api_gen_dict_data
-- ----------------------------
DROP TABLE IF EXISTS `api_gen_dict_data`;
CREATE TABLE `api_gen_dict_data`  (
  `dict_code` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典编码',
  `dict_sort` int(4) NULL DEFAULT 0 COMMENT '字典排序',
  `dict_label` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '字典标签',
  `dict_value` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL COMMENT '字典键值',
  `dict_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '字典类型',
  `css_class` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '样式属性（其他样式扩展）',
  `list_class` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '表格回显样式',
  `is_default` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT 'N' COMMENT '是否默认（Y是 N否）',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 192 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = 'api字典数据表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of api_gen_dict_data
-- ----------------------------
INSERT INTO `api_gen_dict_data` VALUES (100, 1, 'api入参', '1', 'value_source_add', NULL, NULL, 'N', '0', '', NULL, '', NULL, '添加-字段-值来源');
INSERT INTO `api_gen_dict_data` VALUES (107, 1, '等于', '1', 'condition_type', NULL, NULL, 'N', '0', '', NULL, '', NULL, NULL);
INSERT INTO `api_gen_dict_data` VALUES (108, 2, '不等于', '2', 'condition_type', NULL, NULL, 'N', '0', '', NULL, '', NULL, NULL);
INSERT INTO `api_gen_dict_data` VALUES (109, 3, '大于', '3', 'condition_type', NULL, NULL, 'N', '0', '', NULL, '', NULL, NULL);
INSERT INTO `api_gen_dict_data` VALUES (110, 4, '小于', '4', 'condition_type', NULL, NULL, 'N', '0', '', NULL, '', NULL, NULL);
INSERT INTO `api_gen_dict_data` VALUES (111, 5, '大于等于', '5', 'condition_type', NULL, NULL, 'N', '0', '', NULL, '', NULL, NULL);
INSERT INTO `api_gen_dict_data` VALUES (112, 6, '小于等于', '6', 'condition_type', NULL, NULL, 'N', '0', '', NULL, '', NULL, NULL);
INSERT INTO `api_gen_dict_data` VALUES (117, 11, '全模糊', '11', 'condition_type', NULL, NULL, 'N', '0', '', NULL, '', NULL, NULL);
INSERT INTO `api_gen_dict_data` VALUES (118, 12, '左模糊', '12', 'condition_type', NULL, NULL, 'N', '0', '', NULL, '', NULL, NULL);
INSERT INTO `api_gen_dict_data` VALUES (119, 13, '右模糊', '13', 'condition_type', NULL, NULL, 'N', '0', '', NULL, '', NULL, NULL);
INSERT INTO `api_gen_dict_data` VALUES (120, 1, 'string', 'string', 'parameter_type', NULL, NULL, 'N', '0', '', NULL, '', NULL, NULL);
INSERT INTO `api_gen_dict_data` VALUES (121, 2, 'integer', 'integer', 'parameter_type', NULL, NULL, 'N', '0', '', NULL, '', NULL, NULL);
INSERT INTO `api_gen_dict_data` VALUES (122, 3, 'double', 'double', 'parameter_type', NULL, NULL, 'N', '0', '', NULL, '', NULL, NULL);
INSERT INTO `api_gen_dict_data` VALUES (123, 4, 'float', 'float', 'parameter_type', NULL, NULL, 'N', '0', '', NULL, '', NULL, NULL);
INSERT INTO `api_gen_dict_data` VALUES (124, 1, '是', '1', 'is_required', NULL, NULL, 'N', '0', '', NULL, '', NULL, '是否必填');
INSERT INTO `api_gen_dict_data` VALUES (125, 2, '否', '0', 'is_required', NULL, NULL, 'N', '0', '', NULL, '', NULL, '是否必填');
INSERT INTO `api_gen_dict_data` VALUES (126, 1, '查询返回通用格式', '{\"total\":0,\"rows\":[],\"code\":0,\"msg\":null}', 'SELECT', NULL, NULL, 'N', '0', '', NULL, '', NULL, NULL);
INSERT INTO `api_gen_dict_data` VALUES (127, 2, '创建出参显示', '[{\"parameter\":\"code\",\"parameterName\":\"响应码\",\"parameterType\":\"integer\"},{\"parameter\":\"msg\",\"parameterName\":\"系统消息\",\"parameterType\":\"string\"}]', 'CREATE', NULL, NULL, 'N', '0', '', NULL, '', NULL, NULL);
INSERT INTO `api_gen_dict_data` VALUES (128, 3, '修改出参显示', '[{\"parameter\":\"code\",\"parameterName\":\"响应码\",\"parameterType\":\"integer\"},{\"parameter\":\"msg\",\"parameterName\":\"系统消息\",\"parameterType\":\"string\"}]', 'UPDATE', NULL, NULL, 'N', '0', '', NULL, '', NULL, NULL);
INSERT INTO `api_gen_dict_data` VALUES (129, 0, '默认组', '0', 'api_group_type', NULL, 'primary', 'N', '0', '', NULL, '', '2023-08-27 11:53:58', NULL);
INSERT INTO `api_gen_dict_data` VALUES (130, 0, '默认版本', '1.0', 'api_version_type', NULL, NULL, 'N', '0', '', '2023-07-03 00:33:44', '', '2023-07-03 00:33:44', NULL);
INSERT INTO `api_gen_dict_data` VALUES (132, 1, 'MYSQL', '{\n  \"databaseType\": \"mysql\",\n  \"host\": \"xxx.xxx.xxx.xxx\",\n  \"port\": \"3306\",\n  \"database\": \"api-generation\",\n  \"username\": \"root\",\n  \"password\": \"xxx\"\n}', 'data_source_type', NULL, NULL, 'N', '1', '', '2023-07-09 15:33:18', '', '2023-08-25 15:44:21', 'mysql数据源');
INSERT INTO `api_gen_dict_data` VALUES (133, 1, '新增', 'CREATE', 'api_action_type', NULL, NULL, 'N', '0', '', '2023-07-15 10:48:30', '', '2023-07-15 10:48:30', 'sql执行动作');
INSERT INTO `api_gen_dict_data` VALUES (134, 2, '修改', 'UPDATE', 'api_action_type', NULL, NULL, 'N', '0', '', '2023-07-15 10:48:30', '', '2023-07-15 10:48:30', 'sql执行动作');
INSERT INTO `api_gen_dict_data` VALUES (135, 3, '查询', 'SELECT', 'api_action_type', NULL, NULL, 'N', '0', '', '2023-07-15 10:48:30', '', '2023-07-15 10:48:30', 'sql执行动作');
INSERT INTO `api_gen_dict_data` VALUES (136, 4, '导出', 'EXPORT', 'api_action_type', NULL, NULL, 'N', '0', '', '2023-07-15 10:48:30', '', '2023-07-15 10:48:30', 'sql执行动作');
INSERT INTO `api_gen_dict_data` VALUES (142, 1, 'csv', 'csv', 'file_type', NULL, NULL, 'N', '0', '', NULL, '', NULL, NULL);
INSERT INTO `api_gen_dict_data` VALUES (143, 2, 'xls', 'xls', 'file_type', NULL, NULL, 'N', '0', '', NULL, '', NULL, NULL);
INSERT INTO `api_gen_dict_data` VALUES (144, 3, 'xlsx', 'xlsx', 'file_type', NULL, NULL, 'N', '0', '', NULL, '', NULL, NULL);
INSERT INTO `api_gen_dict_data` VALUES (145, 1, 'and', 'and', 'logical_type', NULL, NULL, 'N', '0', '', '2023-07-15 18:29:58', '', '2023-07-15 18:29:58', NULL);
INSERT INTO `api_gen_dict_data` VALUES (146, 2, 'or', 'or', 'logical_type', NULL, NULL, 'N', '0', '', '2023-07-15 18:29:58', '', '2023-07-15 18:29:58', NULL);
INSERT INTO `api_gen_dict_data` VALUES (150, 1, '时间(yyyyMMddHHmmss)', '8', 'value_source_export', NULL, NULL, 'N', '0', '', '2023-07-18 23:02:25', '', '2023-07-18 23:02:25', '导出-文件名-值来源');
INSERT INTO `api_gen_dict_data` VALUES (152, 4, '日期(yyyy-MM-dd)', '3', 'value_source_export', NULL, NULL, 'N', '0', '', '2023-07-18 23:03:53', '', '2023-07-18 23:03:53', '导出-文件名-值来源');
INSERT INTO `api_gen_dict_data` VALUES (154, 5, '日期(yyyyMMdd)', '9', 'value_source_export', NULL, NULL, 'N', '0', '', '2023-07-18 23:04:17', '', '2023-07-18 23:04:17', '导出-文件名-值来源');
INSERT INTO `api_gen_dict_data` VALUES (156, 8, 'UUID', '4', 'value_source_export', NULL, NULL, 'N', '0', '', '2023-07-18 23:04:45', '', '2023-08-26 14:18:00', '导出-文件名-值来源');
INSERT INTO `api_gen_dict_data` VALUES (157, 2, 'CLICKHOUSE', '{\n  \"databaseType\": \"clickhouse\",\n  \"host\": \"xxx.xxx.xxx.xxx\",\n  \"port\": \"8123\",\n  \"database\": \"aiop\",\n  \"username\": \"default\",\n  \"password\": \"xxx\"\n}', 'data_source_type', NULL, NULL, 'N', '1', '', '2023-07-09 15:33:18', '', '2023-08-27 17:02:49', 'ClickHouse数据源');
INSERT INTO `api_gen_dict_data` VALUES (166, 3, 'OPENGAUSS', '{\n  \"databaseType\": \"opengauss\",\n  \"host\": \"xxx.xxx.xxx.xxx\",\n  \"port\": \"25400\",\n  \"database\": \"mydb\",\n  \"username\": \"opengauss\",\n  \"password\": \"xxx\"\n}', 'data_source_type', NULL, NULL, 'N', '1', '', '2023-07-09 15:33:18', '', '2023-08-27 17:57:46', 'openGauss数据源');
INSERT INTO `api_gen_dict_data` VALUES (169, 2, '时间(yyyyMMddHHmm)', '10', 'value_source_export', NULL, NULL, 'N', '0', '', '2023-07-18 23:02:25', '', '2023-07-18 23:02:25', '导出-文件名-值来源');
INSERT INTO `api_gen_dict_data` VALUES (170, 3, '时间(yyyyMMddHH)', '11', 'value_source_export', NULL, NULL, 'N', '0', '', '2023-07-18 23:02:25', '', '2023-07-18 23:02:25', '导出-文件名-值来源');
INSERT INTO `api_gen_dict_data` VALUES (171, 6, '月份(yyyyMM)', '12', 'value_source_export', NULL, NULL, 'N', '0', '', '2023-07-18 23:02:25', '', '2023-07-18 23:02:25', '导出-文件名-值来源');
INSERT INTO `api_gen_dict_data` VALUES (172, 7, '年份(yyyy)', '13', 'value_source_export', NULL, NULL, 'N', '0', '', '2023-07-18 23:02:25', '', '2023-07-18 23:02:25', '导出-文件名-值来源');
INSERT INTO `api_gen_dict_data` VALUES (174, 1, '操作成功', '200', 'responsecode_type', NULL, 'default', 'N', '0', '', '2023-08-26 13:45:58', '', '2023-08-26 13:46:04', NULL);
INSERT INTO `api_gen_dict_data` VALUES (175, 2, '对象创建成功', '201', 'responsecode_type', NULL, 'default', 'N', '0', '', '2023-08-26 13:46:20', '', '2023-08-26 13:46:19', NULL);
INSERT INTO `api_gen_dict_data` VALUES (176, 3, '请求已经被接受', '202', 'responsecode_type', NULL, 'default', 'N', '0', '', '2023-08-26 13:46:34', '', '2023-08-26 13:46:33', NULL);
INSERT INTO `api_gen_dict_data` VALUES (177, 4, '操作已经执行成功，但是没有返回数据', '204', 'responsecode_type', NULL, 'default', 'N', '0', '', '2023-08-26 13:46:56', '', '2023-08-26 13:46:56', NULL);
INSERT INTO `api_gen_dict_data` VALUES (178, 5, '资源已被移除', '301', 'responsecode_type', NULL, 'default', 'N', '0', '', '2023-08-26 13:47:09', '', '2023-08-26 13:47:09', NULL);
INSERT INTO `api_gen_dict_data` VALUES (179, 6, '重定向', '303', 'responsecode_type', NULL, 'default', 'N', '0', '', '2023-08-26 13:47:23', '', '2023-08-26 13:47:22', NULL);
INSERT INTO `api_gen_dict_data` VALUES (180, 7, '资源没有被修改', '304', 'responsecode_type', NULL, 'default', 'N', '0', '', '2023-08-26 13:47:35', '', '2023-08-26 13:47:34', NULL);
INSERT INTO `api_gen_dict_data` VALUES (181, 8, '参数列表错误（缺少，格式不匹配）', '400', 'responsecode_type', NULL, 'default', 'N', '0', '', '2023-08-26 13:47:58', '', '2023-08-26 13:47:58', NULL);
INSERT INTO `api_gen_dict_data` VALUES (182, 9, '未授权', '401', 'responsecode_type', NULL, 'default', 'N', '0', '', '2023-08-26 13:48:08', '', '2023-08-26 13:48:07', NULL);
INSERT INTO `api_gen_dict_data` VALUES (183, 10, '访问受限，授权过期', '403', 'responsecode_type', NULL, 'default', 'N', '0', '', '2023-08-26 13:48:20', '', '2023-08-26 13:48:19', NULL);
INSERT INTO `api_gen_dict_data` VALUES (184, 11, '资源、服务未找到', '404', 'responsecode_type', NULL, 'default', 'N', '0', '', '2023-08-26 13:48:47', '', '2023-08-26 13:48:46', NULL);
INSERT INTO `api_gen_dict_data` VALUES (185, 12, '不允许的http方法', '405', 'responsecode_type', NULL, 'default', 'N', '0', '', '2023-08-26 13:49:03', '', '2023-08-26 13:49:03', NULL);
INSERT INTO `api_gen_dict_data` VALUES (186, 13, '资源冲突，或者资源被锁', '409', 'responsecode_type', NULL, 'default', 'N', '0', '', '2023-08-26 13:49:18', '', '2023-08-26 13:49:18', NULL);
INSERT INTO `api_gen_dict_data` VALUES (187, 14, '不支持的数据、媒体类型', '415', 'responsecode_type', NULL, 'default', 'N', '0', '', '2023-08-26 13:49:49', '', '2023-08-26 13:49:54', NULL);
INSERT INTO `api_gen_dict_data` VALUES (188, 15, '系统内部错误', '500', 'responsecode_type', NULL, 'default', 'N', '0', '', '2023-08-26 13:50:05', '', '2023-08-26 13:50:04', NULL);
INSERT INTO `api_gen_dict_data` VALUES (189, 16, '接口未实现', '501', 'responsecode_type', NULL, 'default', 'N', '0', '', '2023-08-26 13:50:16', '', '2023-08-26 13:50:22', NULL);
INSERT INTO `api_gen_dict_data` VALUES (190, 17, '系统警告消息', '601', 'responsecode_type', NULL, 'default', 'N', '0', '', '2023-08-26 13:50:36', '', '2023-08-26 13:50:35', NULL);
INSERT INTO `api_gen_dict_data` VALUES (191, 1, 'clickhouse', '1', 'api_group_type', NULL, 'primary', 'N', '0', '', '2023-08-27 11:53:05', '', '2023-08-27 11:54:06', NULL);
INSERT INTO `api_gen_dict_data` VALUES (192, 2, 'opengauss', '2', 'api_group_type', NULL, 'primary', 'N', '0', '', '2023-08-27 15:22:07', '', '2023-08-27 15:22:37', NULL);

-- ----------------------------
-- Table structure for api_gen_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `api_gen_dict_type`;
CREATE TABLE `api_gen_dict_type`  (
  `dict_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典主键',
  `dict_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '字典名称',
  `dict_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '字典类型',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_id`) USING BTREE,
  UNIQUE INDEX `dict_type`(`dict_type`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 104 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '字典类型表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of api_gen_dict_type
-- ----------------------------
INSERT INTO `api_gen_dict_type` VALUES (1, '数据源', 'data_source_type', '0', 'admin', '2023-06-18 11:08:20', '', NULL, '数据源列表');
INSERT INTO `api_gen_dict_type` VALUES (2, '执行动作', 'api_action_type', '0', 'admin', '2023-06-18 11:08:20', '', NULL, '执行动作列表');
INSERT INTO `api_gen_dict_type` VALUES (3, 'API分组', 'api_group_type', '0', 'admin', '2023-06-18 11:08:20', '', NULL, 'API分组列表');
INSERT INTO `api_gen_dict_type` VALUES (5, 'API版本', 'api_version_type', '0', 'admin', '2023-06-18 11:08:20', '', NULL, 'API版本列表');
INSERT INTO `api_gen_dict_type` VALUES (6, '条件类型', 'condition_type', '0', 'admin', '2023-06-18 11:08:20', '', NULL, '条件类型列表');
INSERT INTO `api_gen_dict_type` VALUES (7, '文件类型', 'file_type', '0', 'admin', '2023-06-18 11:08:20', '', NULL, '文件类型列表');
INSERT INTO `api_gen_dict_type` VALUES (8, '必填类型', 'is_required', '0', 'admin', '2023-06-18 11:08:20', '', NULL, '必填类型列表');
INSERT INTO `api_gen_dict_type` VALUES (9, '逻辑类型', 'logical_type', '0', 'admin', '2023-06-18 11:08:20', '', NULL, '逻辑类型列表');
INSERT INTO `api_gen_dict_type` VALUES (10, '数据类型', 'parameter_type', '0', 'admin', '2023-06-18 11:08:20', '', NULL, '数据类型列表');
INSERT INTO `api_gen_dict_type` VALUES (11, 'API入参类型', 'value_source_add', '0', 'admin', '2023-06-18 11:08:20', '', NULL, 'API入参列表');
INSERT INTO `api_gen_dict_type` VALUES (12, '报表后缀类型', 'value_source_export', '0', 'admin', '2023-06-18 11:08:20', '', NULL, '报表后缀列表');
INSERT INTO `api_gen_dict_type` VALUES (13, '创建出参', 'CREATE', '0', 'admin', '2023-06-18 11:08:20', '', NULL, '创建出参列表');
INSERT INTO `api_gen_dict_type` VALUES (14, '查询出参', 'SELECT', '0', 'admin', '2023-06-18 11:08:20', '', NULL, '查询出参列表');
INSERT INTO `api_gen_dict_type` VALUES (15, '修改出参', 'UPDATE', '0', 'admin', '2023-06-18 11:08:20', '', NULL, '修改出参列表');
INSERT INTO `api_gen_dict_type` VALUES (101, '响应码类型', 'responsecode_type', '0', 'admin', '2023-08-26 13:43:50', '', NULL, '响应码类型列表');

-- ----------------------------
-- Table structure for api_gen_execute
-- ----------------------------
DROP TABLE IF EXISTS `api_gen_execute`;
CREATE TABLE `api_gen_execute`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `api_id` bigint(20) NOT NULL COMMENT 'api的id',
  `database_type` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT 'MYSQL' COMMENT '数据库类型',
  `database_name` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '数据库名',
  `table_name` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '表名',
  `action` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '操作类型 CREATE、UPDATE、SELECT、EXPORT',
  `conditional` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL COMMENT '条件json格式类型',
  `update_field` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL COMMENT '新增、更新字段json格式类型',
  `sql_statement` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL COMMENT 'sql语句，当操作类型为SELECT、EXPORT时才会有值',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `api_id_unique`(`api_id`) USING BTREE COMMENT 'api_id的执行唯一'
) ENGINE = InnoDB AUTO_INCREMENT = 32 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = 'api执行' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for api_gen_info
-- ----------------------------
DROP TABLE IF EXISTS `api_gen_info`;
CREATE TABLE `api_gen_info`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `api_action` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '操作类型 CREATE、UPDATE、SELECT、EXPORT',
  `api_path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT 'api请求路径',
  `api_path_show` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT 'api请求路径页面显示用',
  `api_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT 'api名称',
  `api_type` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT 'POST' COMMENT 'api请求类型',
  `api_illustrate` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT 'api说明',
  `api_group` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '0' COMMENT 'api分组',
  `api_version` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '1.0' COMMENT 'api版本',
  `api_header` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '请求头（0 启用 1未启用）',
  `api_publish` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '1' COMMENT '是否发布（0已发布 1未发布）',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `api_path_unique`(`api_path`) USING BTREE COMMENT '确定请求唯一性'
) ENGINE = InnoDB AUTO_INCREMENT = 52 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = 'api服务信息' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
