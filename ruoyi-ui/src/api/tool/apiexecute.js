import request from '@/utils/request'

// 查询api执行列表
export function listExecute(query) {
  return request({
    url: '/apigeneration/execute/list',
    method: 'get',
    params: query
  })
}

// 查询api执行详细
export function getExecute(id) {
  return request({
    url: '/apigeneration/execute/' + id,
    method: 'get'
  })
}

// 根据apiId查询api执行详细
export function getExecuteByApiId(apiId) {
  return request({
    url: '/apigeneration/execute/apiid/' + apiId,
    method: 'get'
  })
}

// 新增api执行
export function addExecute(data) {
  return request({
    url: '/apigeneration/execute',
    method: 'post',
    data: data
  })
}

// 修改api执行
export function updateExecute(data) {
  return request({
    url: '/apigeneration/execute',
    method: 'put',
    data: data
  })
}

// 删除api执行
export function delExecute(id) {
  return request({
    url: '/apigeneration/execute/' + id,
    method: 'delete'
  })
}

// 根据数据库类型获取数据库中的库名
export function getDBType(dbtype) {
  return request({
    url: '/apigeneration/execute/dbtype/' + dbtype,
    method: 'get'
  })
}

// 根据数据库类型和数据库名获取数据库中的表名
export function getDBTypeDBName(dbtype,dbname) {
  return request({
    url: '/apigeneration/execute/dbtype/' + dbtype+"/"+dbname,
    method: 'get'
  })
}

// 根据数据库类型和数据库名和表名获取表中的字段
export function getDBTypeDBnameTablename(dbtype,dbname,tablename) {
  return request({
    url: '/apigeneration/execute/dbtype/' + dbtype + "/" + dbname + "/" + tablename,
    method: 'get'
  })
}
