import request from '@/utils/request'

// API服务请求前缀
export function getApiPathPrefix() {
  return request({
    url: '/apigeneration/info/apipathprefix',
    method: 'get'
  })
}

// 查询api服务信息列表
export function listInfo(query) {
  return request({
    url: '/apigeneration/info/list',
    method: 'get',
    params: query
  })
}

// 查询api服务信息详细
export function getInfo(id) {
  return request({
    url: '/apigeneration/info/' + id,
    method: 'get'
  })
}

// 新增api服务信息
export function addInfo(data) {
  return request({
    url: '/apigeneration/info',
    method: 'post',
    data: data
  })
}

// 修改api服务信息
export function updateInfo(data) {
  return request({
    url: '/apigeneration/info',
    method: 'put',
    data: data
  })
}

// 修改api发布状态
export function updateInfoApipublish(id, apiPublish) {
  const data = {
    id,
    apiPublish
  }
  return request({
    url: '/apigeneration/info',
    method: 'put',
    data: data
  })
}
// 修改api服务信息状态
export function changeApiStatus(id, status) {
  const data = {
    id,
    status
  }
  return request({
    url: '/apigeneration/info',
    method: 'put',
    data: data
  })
}

// 删除api服务信息
export function delInfo(id) {
  return request({
    url: '/apigeneration/info/' + id,
    method: 'delete'
  })
}
