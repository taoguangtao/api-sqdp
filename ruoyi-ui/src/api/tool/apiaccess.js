import request from '@/utils/request'

// 查询api出入参列表
export function listAccess(query) {
  return request({
    url: '/apigeneration/access/list',
    method: 'get',
    params: query
  })
}

// 查询api出入参详细
export function getAccess(id) {
  return request({
    url: '/apigeneration/access/' + id,
    method: 'get'
  })
}

// 根据apiId查询api出入参详细
export function getInfoByApi(apiId) {
  return request({
    url: '/apigeneration/access/byapi/' + apiId,
    method: 'get'
  })
}


// 新增api出入参
export function addAccess(data) {
  return request({
    url: '/apigeneration/access',
    method: 'post',
    data: data
  })
}

// 修改api出入参
export function updateAccess(data) {
  return request({
    url: '/apigeneration/access',
    method: 'put',
    data: data
  })
}

// 根据apiId修改api出入参
export function updateAccessByApiId(data) {
  return request({
    url: '/apigeneration/access/byapiid',
    method: 'put',
    data: data
  })
}

// 删除api出入参
export function delAccess(id) {
  return request({
    url: '/apigeneration/access/' + id,
    method: 'delete'
  })
}
