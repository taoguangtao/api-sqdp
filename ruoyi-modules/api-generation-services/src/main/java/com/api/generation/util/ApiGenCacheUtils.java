package com.api.generation.util;

import com.alibaba.fastjson2.JSONArray;
import com.api.generation.domain.ApiGenAccess;
import com.api.generation.domain.ApiGenDictData;
import com.api.generation.domain.ApiGenExecute;
import com.api.generation.domain.ApiGenInfo;
import com.ruoyi.common.core.utils.SpringUtils;
import com.ruoyi.common.core.utils.StringUtils;
import com.ruoyi.common.redis.service.RedisService;

import java.util.Collection;
import java.util.List;
/**
 * 数据服务工具类
 * @author chaogao
 * @date 2023/7/2 23:14
 */
public class ApiGenCacheUtils
{
    /**
     * api服务信息缓存数据 cache key
     */
    public static final String SYS_APIGENINFO_KEY = "apigeninfo:";
    /**
     * api执行缓存数据 cache key
     */
    public static final String SYS_APIGENEXECUTE_KEY = "apigenexecute:";
    /**
     * api出入参数据 cache key
     */
    public static final String SYS_APIGACCESS_KEY = "apigaccess:";
    /**
     * api字典数据 cache key
     */
    public static final String SYS_APIGDICT_KEY = "apigdict:";

    // 设置缓存
    public static void setApiGenInfoCache(String key, ApiGenInfo apiGenInfo)
    {
        SpringUtils.getBean(RedisService.class).setCacheObject(getApiGenInfoCacheKey(key), apiGenInfo);
    }

    public static void setApiGenExecuteCache(String key, ApiGenExecute apiGenExecute)
    {
        SpringUtils.getBean(RedisService.class).setCacheObject(getApiGenExecuteCacheKey(key), apiGenExecute);
    }

    public static void setApiGenAccessCache(String key, ApiGenAccess apiGenAccess)
    {
        SpringUtils.getBean(RedisService.class).setCacheObject(getApiGenAccessCacheKey(key), apiGenAccess);
    }

    public static void setApiGenDictCache(String key,  List<ApiGenDictData> dictDatas)
    {
        SpringUtils.getBean(RedisService.class).setCacheObject(getApiGenDictCacheKey(key), dictDatas);
    }

    // 获取缓存
    public static ApiGenInfo getApiGenInfoCache(String key)
    {
        // JSON.parseObject(jsonString)
        Object cacheObject = SpringUtils.getBean(RedisService.class).getCacheObject(getApiGenInfoCacheKey(key));
        if (StringUtils.isNotNull(cacheObject))
        {
            return (ApiGenInfo) cacheObject;
        }
        return null;
    }


    public static ApiGenExecute getApiGenExecuteCache(String key)
    {
        Object cacheObject = SpringUtils.getBean(RedisService.class).getCacheObject(getApiGenExecuteCacheKey(key));
        if (StringUtils.isNotNull(cacheObject))
        {
            return (ApiGenExecute) cacheObject;
        }
        return null;
    }

    public static ApiGenAccess getApiGenAccessCache(String key)
    {
        Object cacheObject = SpringUtils.getBean(RedisService.class).getCacheObject(getApiGenAccessCacheKey(key));
        if (StringUtils.isNotNull(cacheObject))
        {
            return (ApiGenAccess) cacheObject;
        }
        return null;
    }

    public static List<ApiGenDictData> getApiGenDictCache(String key)
    {
        JSONArray arrayCache = SpringUtils.getBean(RedisService.class).getCacheObject(getApiGenDictCacheKey(key));
        if (StringUtils.isNotNull(arrayCache))
        {
            return arrayCache.toList(ApiGenDictData.class);
        }
        return null;
    }

    // 删除缓存
    public static void removeApiGenInfoCache(String key)
    {
        SpringUtils.getBean(RedisService.class).deleteObject(getApiGenInfoCacheKey(key));
    }

    public static void removeApiGenExecuteCache(String key)
    {
        SpringUtils.getBean(RedisService.class).deleteObject(getApiGenExecuteCacheKey(key));
    }

    public static void removeApiGenAccessCache(String key)
    {
        SpringUtils.getBean(RedisService.class).deleteObject(getApiGenAccessCacheKey(key));
    }

    public static void removeApiGenDictCache(String key)
    {
        SpringUtils.getBean(RedisService.class).deleteObject(getApiGenDictCacheKey(key));
    }

    // 清空缓存
    public static void clearApiGenInfoCache()
    {
        Collection<String> keys = SpringUtils.getBean(RedisService.class).keys(SYS_APIGENINFO_KEY + "*");
        SpringUtils.getBean(RedisService.class).deleteObject(keys);
    }

    public static void clearApiGenExecuteCache()
    {
        Collection<String> keys = SpringUtils.getBean(RedisService.class).keys(SYS_APIGENEXECUTE_KEY + "*");
        SpringUtils.getBean(RedisService.class).deleteObject(keys);
    }

    public static void clearApiGenAccessCache()
    {
        Collection<String> keys = SpringUtils.getBean(RedisService.class).keys(SYS_APIGACCESS_KEY + "*");
        SpringUtils.getBean(RedisService.class).deleteObject(keys);
    }

    public static void clearApiGenDictCache()
    {
        Collection<String> keys = SpringUtils.getBean(RedisService.class).keys(SYS_APIGDICT_KEY + "*");
        SpringUtils.getBean(RedisService.class).deleteObject(keys);
    }

    // 设置缓存key
    public static String getApiGenInfoCacheKey(String configKey)
    {
        return SYS_APIGENINFO_KEY + configKey;
    }

    public static String getApiGenExecuteCacheKey(String configKey)
    {
        return SYS_APIGENEXECUTE_KEY + configKey;
    }

    public static String getApiGenAccessCacheKey(String configKey)
    {
        return SYS_APIGACCESS_KEY + configKey;
    }

    public static String getApiGenDictCacheKey(String configKey)
    {
        return SYS_APIGDICT_KEY + configKey;
    }
}
