package com.api.generation.service.impl;

import java.util.List;

import com.api.generation.service.IApiGenService;
import com.api.generation.util.ApiGenCacheUtils;
import com.ruoyi.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.api.generation.mapper.ApiGenAccessMapper;
import com.api.generation.domain.ApiGenAccess;
import com.api.generation.service.IApiGenAccessService;

/**
 * api出入参Service业务层处理
 * 
 * @author gaochao
 * @date 2023-07-15
 */
@Service
public class ApiGenAccessServiceImpl implements IApiGenAccessService 
{
    @Autowired
    private ApiGenAccessMapper apiGenAccessMapper;
    @Autowired
    private IApiGenService iApiGenService;

    /**
     * 查询api出入参
     * 
     * @param id api出入参主键
     * @return api出入参
     */
    @Override
    public ApiGenAccess selectApiGenAccessById(Long id)
    {
        return iApiGenService.getApiGenAccessCache(id.toString());
    }

    /**
     * 查询api出入参列表
     * 
     * @param apiGenAccess api出入参
     * @return api出入参
     */
    @Override
    public List<ApiGenAccess> selectApiGenAccessList(ApiGenAccess apiGenAccess)
    {
        return apiGenAccessMapper.selectApiGenAccessList(apiGenAccess);
    }

    /**
     * 新增api出入参
     * 
     * @param apiGenAccess api出入参
     * @return 结果
     */
    @Override
    public int insertApiGenAccess(ApiGenAccess apiGenAccess)
    {
        apiGenAccess.setCreateTime(DateUtils.getNowDate());
        int i = apiGenAccessMapper.insertApiGenAccess(apiGenAccess);
        ApiGenAccess apiGenAccess1 = apiGenAccessMapper.selectApiGenAccessById(apiGenAccess.getId());
        ApiGenCacheUtils.setApiGenAccessCache(apiGenAccess1.getId().toString(),apiGenAccess1);
        return i;
    }

    /**
     * 修改api出入参
     * 
     * @param apiGenAccess api出入参
     * @return 结果
     */
    @Override
    public int updateApiGenAccess(ApiGenAccess apiGenAccess)
    {
        apiGenAccess.setUpdateTime(DateUtils.getNowDate());
        int i = apiGenAccessMapper.updateApiGenAccess(apiGenAccess);
        ApiGenAccess apiGenAccess1 = apiGenAccessMapper.selectApiGenAccessById(apiGenAccess.getId());
        ApiGenCacheUtils.setApiGenAccessCache(apiGenAccess1.getId().toString(),apiGenAccess1);
        return i;
    }

    /**
     * 批量删除api出入参
     * 
     * @param ids 需要删除的api出入参主键
     * @return 结果
     */
    @Override
    public int deleteApiGenAccessByIds(Long[] ids)
    {
        int i = apiGenAccessMapper.deleteApiGenAccessByIds(ids);
        iApiGenService.resetApiGenAccessCache();
        for (int k = 0; k < ids.length; k++) {
            ApiGenCacheUtils.removeApiGenAccessCache(ids[k].toString());
        }
        return i;
    }

    /**
     * 删除api出入参信息
     * 
     * @param id api出入参主键
     * @return 结果
     */
    @Override
    public int deleteApiGenAccessById(Long id)
    {
        int i = apiGenAccessMapper.deleteApiGenAccessById(id);
        ApiGenCacheUtils.removeApiGenAccessCache(id.toString());
        return i;
    }

    @Override
    public ApiGenAccess selectApiGenAccessByApiId(Long apiId) {
        return apiGenAccessMapper.selectApiGenAccessByApiId(apiId);
    }

    @Override
    public int updateAccessByApiId(ApiGenAccess apiGenAccess) {
        apiGenAccess.setUpdateTime(DateUtils.getNowDate());
        int i = apiGenAccessMapper.updateAccessByApiId(apiGenAccess);
        ApiGenAccess apiGenAccess1 = apiGenAccessMapper.selectApiGenAccessByApiId(apiGenAccess.getApiId());
        ApiGenCacheUtils.setApiGenAccessCache(apiGenAccess1.getId().toString(),apiGenAccess1);
        return i;
    }
}
