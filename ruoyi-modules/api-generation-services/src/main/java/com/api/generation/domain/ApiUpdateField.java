package com.api.generation.domain;


import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 新增、更新实体类型
 */
@Data
@AllArgsConstructor
public class ApiUpdateField {

    private static final long serialVersionUID = 1L;

    /** 是否主键  1:是,0:否*/
/*
    private Integer isPrimary;
*/

    /** 参数*/
    private String parameter;

    /** 字段名称*/
    private String fieldName;

    /** 参数类型
     1:api入参
     2:系统时间(yyyy-MM-dd HH:mm:ss)
     3:系统日期(yyyy-MM-dd)
     4:系统UUID
     5:自定义
     6:数据库自增
     7:设置为空值(NULL)
     系统时间(yyyyMMddHHmmss)
     系统时间(yyyyMMdd)*/
    private Integer valueSource;

    /** 参数类型
     string
     integer
     number
     boolean*/
  /*  private String parameterType;*/
}