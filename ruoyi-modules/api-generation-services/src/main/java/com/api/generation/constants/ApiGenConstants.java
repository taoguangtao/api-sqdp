package com.api.generation.constants;


/**
 * 数据服务通用接口常量信息
 * @author chaogao
 * @date 2023/7/9 10:50
 */
public class ApiGenConstants
{
    // 操作类型
    public final static String CREATE = "create";

    public final static String UPDATE = "update";

    public final static String SELECT = "select";

    public final static String EXPORT = "export";

    // 入参类型
    public final static String STRING = "string";

    public final static String INTEGER = "integer";

    public final static String DOUBLE = "double";

    public final static String FLOAT = "float";

    // 是否必填 1:是、2:否
    public final static Integer IS_REQUIRED = 1;

    // CREATE mysql、clickhose模板
    public final static String INSERTAPIGEN = " INSERT INTO `%s`.`%s`(%s) VALUES (%s) ";

    // UPDATE mysql、clickhose模板
    public final static String UPDATEAPIGEN = " UPDATE `%s`.`%s` SET %s WHERE %s ";

    public final static String UPDATEAPIGENNOWHERE = " UPDATE `%s`.`%s` SET %s ";

    // 数据源类型 目前支持的数据库有 mysql、clickhouse、高斯(国产数据库)、以实现;达梦、万里 规划中
    public final static String DATA_SOURCE_TYPE = "data_source_type";

    // mysql
    public final static String DATABASE_MYSQL = "mysql";
    // clickhouse
    public final static String DATABASE_CLICKHOUSE = "clickhouse";

    // 默认页数
    public final static Integer PAGENUM=1;

    // 默认每页数据条数
    public final static Integer PAGESIZE=10;

    // 是否带limit 0:否,1:是
    public final static Integer EXPORTISPAG=0;

    // 状态（0正常 1停用）
    public final static Integer DEACTIVATE_STATUS=1;

    // 是否发布（0已发布 1未发布）
    public final static Integer NOT_PUBLISH_STATUS=1;

    // 默认初始导出数据条数
    public final static Integer EXPORTPAGESIZE=1000;

}
