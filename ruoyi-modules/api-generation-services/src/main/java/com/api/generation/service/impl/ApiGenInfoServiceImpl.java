package com.api.generation.service.impl;

import com.api.generation.domain.ApiGenAccess;
import com.api.generation.domain.ApiGenExecute;
import com.api.generation.domain.ApiGenInfo;
import com.api.generation.mapper.ApiGenAccessMapper;
import com.api.generation.mapper.ApiGenExecuteMapper;
import com.api.generation.mapper.ApiGenInfoMapper;
import com.api.generation.service.IApiGenInfoService;
import com.api.generation.service.IApiGenService;
import com.api.generation.util.ApiGenCacheUtils;
import com.ruoyi.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * api服务信息Service业务层处理
 * 
 * @author gaochao
 * @date 2023-07-02
 */
@Service
public class ApiGenInfoServiceImpl implements IApiGenInfoService 
{
    @Autowired
    private ApiGenInfoMapper apiGenInfoMapper;

    @Autowired
    private ApiGenAccessMapper apiGenAccessMapper;

    @Autowired
    private ApiGenExecuteMapper apiGenExecuteMapper;

    @Autowired
    private IApiGenService iApiGenService;

    /**
     * 查询api服务信息
     * 
     * @param id api服务信息主键
     * @return api服务信息
     */
    @Override
    public ApiGenInfo selectApiGenInfoById(Long id)
    {
        return apiGenInfoMapper.selectApiGenInfoById(id);
    }

    /**
     * 查询api服务信息列表
     * 
     * @param apiGenInfo api服务信息
     * @return api服务信息
     */
    @Override
    public List<ApiGenInfo> selectApiGenInfoList(ApiGenInfo apiGenInfo)
    {
        return apiGenInfoMapper.selectApiGenInfoList(apiGenInfo);
    }

    /**
     * 新增api服务信息
     * 
     * @param apiGenInfo api服务信息
     * @return 结果
     */
    @Override
    public Long insertApiGenInfo(ApiGenInfo apiGenInfo)
    {
        apiGenInfo.setCreateTime(DateUtils.getNowDate());
        String apiPath = apiGenInfo.getApiPath();
        String apiGroup = apiGenInfo.getApiGroup();
        String apiVersion = apiGenInfo.getApiVersion();
        apiGenInfo.setApiPath(apiPath+"/"+apiGroup+"/"+apiVersion);
        apiGenInfoMapper.insertApiGenInfo(apiGenInfo);
        ApiGenInfo apiGenInfo1 = apiGenInfoMapper.selectApiGenInfoById(apiGenInfo.getId());
        ApiGenCacheUtils.setApiGenInfoCache(apiGenInfo1.getApiPath(),apiGenInfo1);
        return apiGenInfo.getId();
    }

    /**
     * 修改api服务信息
     * 
     * @param apiGenInfo api服务信息
     * @return 结果
     */
    @Override
    public int updateApiGenInfo(ApiGenInfo apiGenInfo)
    {
        apiGenInfo.setUpdateTime(DateUtils.getNowDate());
        int i = apiGenInfoMapper.updateApiGenInfo(apiGenInfo);
        ApiGenInfo apiGenInfo1 = apiGenInfoMapper.selectApiGenInfoById(apiGenInfo.getId());
        ApiGenCacheUtils.setApiGenInfoCache(apiGenInfo1.getApiPath(),apiGenInfo1);
        return i;
    }

    /**
     * 批量删除api服务信息
     * 
     * @param ids 需要删除的api服务信息主键
     * @return 结果
     */
    @Override
    public int deleteApiGenInfoByIds(Long[] ids)
    {
        // 1. 移除缓存
        for (Long id:ids){
            // api移除缓存和数据库数据
            ApiGenInfo apiGenInfo = apiGenInfoMapper.selectApiGenInfoById(id);
            iApiGenService.removeApiGenInfoCache(apiGenInfo.getApiPath());
            // api出入参移除缓存和数据库数据
            ApiGenAccess apiGenAccess = apiGenAccessMapper.selectApiGenAccessByApiId(id);
            if(null!=apiGenAccess){
                iApiGenService.removeApiGenAccessCache(apiGenAccess.getId().toString());
                apiGenAccessMapper.deleteApiGenAccessById(apiGenAccess.getId());
            }
            // api执行器移除缓存和数据库数据
            ApiGenExecute apiGenExecute = apiGenExecuteMapper.selectApiGenExecuteByApiId(id);
            if(null!=apiGenExecute) {
                iApiGenService.removeApiGenExecuteCache(apiGenExecute.getId().toString());
                apiGenExecuteMapper.deleteApiGenExecuteById(apiGenExecute.getId());
            }
        }
        int i = apiGenInfoMapper.deleteApiGenInfoByIds(ids);
        // 2.删除api出入参
        return i;
    }

    /**
     * 删除api服务信息信息
     * 
     * @param id api服务信息主键
     * @return 结果
     */
    @Override
    public int deleteApiGenInfoById(Long id)
    {
        int i = apiGenInfoMapper.deleteApiGenInfoById(id);
        ApiGenInfo apiGenInfo1 = apiGenInfoMapper.selectApiGenInfoById(id);
        ApiGenCacheUtils.removeApiGenInfoCache(apiGenInfo1.getApiPath());
        return i;
    }

    @Override
    public ApiGenInfo selectApiGenInfoByApiPath(String servletPath) {
        return apiGenInfoMapper.selectApiGenInfoByApiPath(servletPath);
    }
}
