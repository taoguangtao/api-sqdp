package com.api.generation.mapper;

import java.util.List;
import com.api.generation.domain.ApiGenAccess;

/**
 * api出入参Mapper接口
 * 
 * @author gaochao
 * @date 2023-07-15
 */
public interface ApiGenAccessMapper 
{
    /**
     * 查询api出入参
     * 
     * @param id api出入参主键
     * @return api出入参
     */
    public ApiGenAccess selectApiGenAccessById(Long id);

    /**
     * 查询api出入参列表
     * 
     * @param apiGenAccess api出入参
     * @return api出入参集合
     */
    public List<ApiGenAccess> selectApiGenAccessList(ApiGenAccess apiGenAccess);

    /**
     * 新增api出入参
     * 
     * @param apiGenAccess api出入参
     * @return 结果
     */
    public int insertApiGenAccess(ApiGenAccess apiGenAccess);

    /**
     * 修改api出入参
     * 
     * @param apiGenAccess api出入参
     * @return 结果
     */
    public int updateApiGenAccess(ApiGenAccess apiGenAccess);

    /**
     * 删除api出入参
     * 
     * @param id api出入参主键
     * @return 结果
     */
    public int deleteApiGenAccessById(Long id);

    /**
     * 批量删除api出入参
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteApiGenAccessByIds(Long[] ids);

    void updateByApiIdApiGenAccess(ApiGenAccess apiGenAccess);

    ApiGenAccess selectApiGenAccessByApiId(Long apiId);

    public int updateAccessByApiId(ApiGenAccess apiGenAccess);

    public void deleteApiGenAccessByApiId(Long id);
}
