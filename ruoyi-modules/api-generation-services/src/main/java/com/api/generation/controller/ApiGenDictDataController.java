package com.api.generation.controller;

import java.util.ArrayList;
import java.util.List;
import java.io.IOException;
import java.util.Locale;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import com.api.generation.service.IApiGenService;
import com.ruoyi.common.core.utils.StringUtils;
import com.ruoyi.system.api.domain.SysDictData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.api.generation.domain.ApiGenDictData;
import com.api.generation.service.IApiGenDictDataService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * api字典数据Controller
 *
 * @author gaochao
 * @date 2023-07-02
 */
@RestController
@RequestMapping("/dict/data")
public class ApiGenDictDataController extends BaseController
{
    @Autowired
    private IApiGenDictDataService apiGenDictDataService;

    @Autowired
    private IApiGenService iApiGenService;

    /**
     * 查询api字典数据列表
     */
    @RequiresPermissions("generation:dict:list")
    @GetMapping("/list")
    public TableDataInfo list(ApiGenDictData apiGenDictData)
    {
        startPage();
        List<ApiGenDictData> list = apiGenDictDataService.selectApiGenDictDataList(apiGenDictData);
        return getDataTable(list);
    }

    /**
     * 导出api字典数据列表
     */
    @RequiresPermissions("generation:dict:export")
    @Log(title = "api字典数据", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ApiGenDictData apiGenDictData)
    {
        List<ApiGenDictData> list = apiGenDictDataService.selectApiGenDictDataList(apiGenDictData);
        ExcelUtil<ApiGenDictData> util = new ExcelUtil<ApiGenDictData>(ApiGenDictData.class);
        util.exportExcel(response, list, "api字典数据数据");
    }

    @GetMapping(value = "/type/{dictType}")
    public AjaxResult dictType(@PathVariable String dictType)
    {
        List<ApiGenDictData> data = apiGenDictDataService.selectDictDataByType(dictType);
        if (StringUtils.isNull(data))
        {
            data = new ArrayList<ApiGenDictData>();
        }
        return success(data);
    }

    /**
     * 获取api字典数据详细信息
     */
    @RequiresPermissions("generation:dict:query")
    @GetMapping(value = "/{dictCode}")
    public AjaxResult getInfo(@PathVariable("dictCode") Long dictCode)
    {
        return success(apiGenDictDataService.selectApiGenDictDataByDictCode(dictCode));
    }

    /**
     * 新增api字典数据
     */
    @RequiresPermissions("generation:dict:add")
    @Log(title = "api字典数据", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ApiGenDictData apiGenDictData)
    {
        return toAjax(apiGenDictDataService.insertApiGenDictData(apiGenDictData));
    }

    /**
     * 修改api字典数据
     */
    @RequiresPermissions("generation:dict:edit")
    @Log(title = "api字典数据", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ApiGenDictData apiGenDictData)
    {
        // 先保存
        int i = apiGenDictDataService.updateApiGenDictData(apiGenDictData);
        // 再对数据源调试
        String dictType = apiGenDictData.getDictType();
        String dictLabel = apiGenDictData.getDictLabel();
        if("data_source_type".equals(dictType)){
            dictLabel = dictLabel.toLowerCase(Locale.ROOT);
            iApiGenService.debugDataSource(dictLabel);
        }
        return toAjax(i);
    }

    /**
     * 删除api字典数据
     */
    @RequiresPermissions("generation:dict:remove")
    @Log(title = "api字典数据", businessType = BusinessType.DELETE)
	@DeleteMapping("/{dictCodes}")
    public AjaxResult remove(@PathVariable Long[] dictCodes)
    {
        return toAjax(apiGenDictDataService.deleteApiGenDictDataByDictCodes(dictCodes));
    }
}
