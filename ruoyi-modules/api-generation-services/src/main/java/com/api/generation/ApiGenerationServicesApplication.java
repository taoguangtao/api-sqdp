package com.api.generation;

import com.ruoyi.common.security.annotation.EnableRyFeignClients;
import com.ruoyi.common.swagger.annotation.EnableCustomSwagger2;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.context.config.annotation.RefreshScope;

/**
 * @author chaogao
 * @date 2023/6/18 17:54
 * api生成生成服务
 */
@MapperScan(value ={"com.api.**.mapper"})
@EnableCustomSwagger2
@EnableRyFeignClients
@SpringBootApplication
@RefreshScope
public class ApiGenerationServicesApplication
{
    public static void main(String[] args)
    {
        SpringApplication.run(ApiGenerationServicesApplication.class, args);
        System.out.println("(♥◠‿◠)ﾉﾞ  api生成生成服务模块启动成功   ლ(´ڡ`ლ)ﾞ  \n");
    }
}
