package com.api.generation.controller;

import com.api.generation.domain.ApiGenExecute;
import com.api.generation.service.IApiGenExecuteService;
import com.api.generation.service.IApiGenService;
import com.ruoyi.common.core.utils.StringUtils;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * api执行Controller
 * 
 * @author gaochao
 * @date 2023-07-02
 */
@RestController
@RequestMapping("/execute")
public class ApiGenExecuteController extends BaseController
{
    @Autowired
    private IApiGenExecuteService apiGenExecuteService;
    @Autowired
    private IApiGenService iApiGenService;

    /**
     * 查询api执行列表
     */
    @GetMapping("/list")
    public TableDataInfo list(ApiGenExecute apiGenExecute)
    {
        startPage();
        List<ApiGenExecute> list = apiGenExecuteService.selectApiGenExecuteList(apiGenExecute);
        return getDataTable(list);
    }

    /**
     * 导出api执行列表
     */
    @Log(title = "api执行", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ApiGenExecute apiGenExecute)
    {
        List<ApiGenExecute> list = apiGenExecuteService.selectApiGenExecuteList(apiGenExecute);
        ExcelUtil<ApiGenExecute> util = new ExcelUtil<ApiGenExecute>(ApiGenExecute.class);
        util.exportExcel(response, list, "api执行数据");
    }

    /**
     * 获取api执行详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(apiGenExecuteService.selectApiGenExecuteById(id));
    }

    /**
     * 根据apiId查询api执行详细
     */
    @GetMapping(value = "/apiid/{apiId}")
    public AjaxResult getInfoByApiId(@PathVariable("apiId") Long apiId)
    {
        return success(apiGenExecuteService.selectApiGenExecuteByApiId(apiId));
    }


    /**
     * 新增api执行
     */
    @Log(title = "api执行", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ApiGenExecute apiGenExecute)
    {
        return toAjax(apiGenExecuteService.insertApiGenExecute(apiGenExecute));
    }

    /**
     * 修改api执行
     */
    @Log(title = "api执行", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ApiGenExecute apiGenExecute)
    {
        return toAjax(apiGenExecuteService.updateApiGenExecute(apiGenExecute));
    }

    /**
     * 删除api执行
     */
    @Log(title = "api执行", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(apiGenExecuteService.deleteApiGenExecuteByIds(ids));
    }

    /**
     *  根据数据库类型获取数据库中的库名
     * @param dbtype
     * @return
     */
    @GetMapping(value = "/dbtype/{dbtype}")
    public AjaxResult getDBType(@PathVariable String dbtype)
    {
        return iApiGenService.getDBType(dbtype);
    }

    /**
     * 根据数据库类型和数据库名获取数据库中的表名
     * @param dbtype
     * @param dbname
     * @return
     */
    @GetMapping(value = "/dbtype/{dbtype}/{dbname}")
    public AjaxResult getDBType(@PathVariable String dbtype,@PathVariable String dbname)
    {
        return iApiGenService.getDBTypeDBname(dbtype,dbname);
    }

    /**
     * 根据数据库类型和数据库名和表名获取表中的字段
     * @param dbtype
     * @param dbname
     * @param tablename
     * @return
     */
    @GetMapping(value = "/dbtype/{dbtype}/{dbname}/{tablename}")
    public AjaxResult getDBType(@PathVariable String dbtype,@PathVariable String dbname,@PathVariable String tablename)
    {
        return iApiGenService.getDBTypeDBnameTablename(dbtype,dbname,tablename);
    }

}
