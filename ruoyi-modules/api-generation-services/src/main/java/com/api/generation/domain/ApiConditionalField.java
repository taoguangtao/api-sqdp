package com.api.generation.domain;


import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 修改的条件值实体类
 */
@Data
@AllArgsConstructor
public class ApiConditionalField {

    private static final long serialVersionUID = 1L;

    /** 左边字段*/
    private String fieldName;
    /*
    *//** 字段类型*//*
    private String parameterType;*/

    /** 条件判断*/
    private Integer conditionType;

    /** 右值来源
     *  1:api入参
     *  5:自定义
     *  7:设置为空值(NULL)
     * */
    private Integer valueSource;

    /** api字段*/
    private String parameter;

    /** 逻辑运算
     * and,or
     * */
    private String logicalType;
}