package com.api.generation.service;

import java.util.List;
import com.api.generation.domain.ApiGenAccess;

/**
 * api出入参Service接口
 * 
 * @author gaochao
 * @date 2023-07-15
 */
public interface IApiGenAccessService 
{
    /**
     * 查询api出入参
     * 
     * @param id api出入参主键
     * @return api出入参
     */
    public ApiGenAccess selectApiGenAccessById(Long id);

    /**
     * 查询api出入参列表
     * 
     * @param apiGenAccess api出入参
     * @return api出入参集合
     */
    public List<ApiGenAccess> selectApiGenAccessList(ApiGenAccess apiGenAccess);

    /**
     * 新增api出入参
     * 
     * @param apiGenAccess api出入参
     * @return 结果
     */
    public int insertApiGenAccess(ApiGenAccess apiGenAccess);

    /**
     * 修改api出入参
     * 
     * @param apiGenAccess api出入参
     * @return 结果
     */
    public int updateApiGenAccess(ApiGenAccess apiGenAccess);

    /**
     * 批量删除api出入参
     * 
     * @param ids 需要删除的api出入参主键集合
     * @return 结果
     */
    public int deleteApiGenAccessByIds(Long[] ids);

    /**
     * 删除api出入参信息
     * 
     * @param id api出入参主键
     * @return 结果
     */
    public int deleteApiGenAccessById(Long id);

    ApiGenAccess selectApiGenAccessByApiId(Long apiId);

    int updateAccessByApiId(ApiGenAccess apiGenAccess);
}
