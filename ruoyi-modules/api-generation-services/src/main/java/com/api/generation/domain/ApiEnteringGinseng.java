package com.api.generation.domain;


import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 添加,修改,查询,导出 入参定义实体类
 */
@Data
@AllArgsConstructor
public class ApiEnteringGinseng {

    private static final long serialVersionUID = 1L;

    /** 是否必填  1:是,0:否*/
    private Integer isMandatory;

    /** 参数*/
    private String parameter;

    /** 参数名称*/
    private String parameterName;

    /** 参数类型
     string
     integer
     double
     float*/
    private String parameterType;
}