package com.api.generation.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * api出入参对象 api_gen_access
 * 
 * @author gaochao
 * @date 2023-07-15
 */
public class ApiGenAccess extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** api的id */
    @Excel(name = "api的id")
    private Long apiId;

    /** 入参json格式类型 */
    @Excel(name = "入参json格式类型")
    private String enteringGinseng;

    /** 出参json格式类型 */
    @Excel(name = "出参json格式类型")
    private String exoticGinseng;

    /** 报表名称格式 */
    @Excel(name = "报表名称格式")
    private String reportNameFormat;

    /** 状态（0正常 1停用） */
    @Excel(name = "状态", readConverterExp = "0=正常,1=停用")
    private String status;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setApiId(Long apiId) 
    {
        this.apiId = apiId;
    }

    public Long getApiId() 
    {
        return apiId;
    }
    public void setEnteringGinseng(String enteringGinseng) 
    {
        this.enteringGinseng = enteringGinseng;
    }

    public String getEnteringGinseng() 
    {
        return enteringGinseng;
    }
    public void setExoticGinseng(String exoticGinseng) 
    {
        this.exoticGinseng = exoticGinseng;
    }

    public String getExoticGinseng() 
    {
        return exoticGinseng;
    }
    public void setReportNameFormat(String reportNameFormat) 
    {
        this.reportNameFormat = reportNameFormat;
    }

    public String getReportNameFormat() 
    {
        return reportNameFormat;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("apiId", getApiId())
            .append("enteringGinseng", getEnteringGinseng())
            .append("exoticGinseng", getExoticGinseng())
            .append("reportNameFormat", getReportNameFormat())
            .append("status", getStatus())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
