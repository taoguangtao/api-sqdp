package com.api.generation.domain;


import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 导出报表名称格式实体类型
 */
@Data
@AllArgsConstructor
public class ApiReportConfig {

    private static final long serialVersionUID = 1L;

    /** 文件类型
     * csv
     * xls
     * xlsx
     * */
    private String fileType;

    /** 固定值*/
    private String fixedValue;

    /** 动态值
     * 系统时间(yyyyMMddHHmmss)
     * 系统日期(yyyy-MM-dd)
     * 系统时间(yyyyMMdd)
     * 系统UUID
     * */
    private Integer valueSource;

}