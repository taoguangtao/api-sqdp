package com.api.generation.domain;


import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 查询、导出出参实体类
 */
@Data
@AllArgsConstructor
public class ApiExoticField {

    private static final long serialVersionUID = 1L;

    /** api字段*/
    private String parameter;

    /** 导出、字段名称*/
    private String parameterName;

    /** 字段类型*/
    private String parameterType;
}