package com.api.generation.service.impl;

import com.api.generation.constants.ApiGenConstants;
import com.api.generation.domain.ApiGenAccess;
import com.api.generation.domain.ApiGenDictData;
import com.api.generation.domain.ApiGenExecute;
import com.api.generation.mapper.ApiGenAccessMapper;
import com.api.generation.mapper.ApiGenExecuteMapper;
import com.api.generation.service.IApiGenExecuteService;
import com.api.generation.service.IApiGenService;
import com.api.generation.util.ApiGenCacheUtils;
import com.api.generation.util.SqlStatementEscapeReductionUtils;
import com.ruoyi.common.core.utils.DateUtils;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Locale;

/**
 * api执行Service业务层处理
 * 
 * @author gaochao
 * @date 2023-07-02
 */
@Log4j2
@Service
public class ApiGenExecuteServiceImpl implements IApiGenExecuteService 
{
    @Autowired
    private ApiGenExecuteMapper apiGenExecuteMapper;
    @Autowired
    private IApiGenService iApiGenService;
    @Autowired
    private ApiGenAccessMapper apiGenAccessMapper;

    /**
     * 查询api执行
     * 
     * @param id api执行主键
     * @return api执行
     */
    @Override
    public ApiGenExecute selectApiGenExecuteById(Long id)
    {
        return iApiGenService.getApiGenExecuteCache(id.toString());
    }

    @Override
    public ApiGenExecute selectApiGenExecuteByApiId(Long apiId)
    {
        return apiGenExecuteMapper.selectApiGenExecuteByApiId(apiId);
    }
    /**
     * 查询api执行列表
     * 
     * @param apiGenExecute api执行
     * @return api执行
     */
    @Override
    public List<ApiGenExecute> selectApiGenExecuteList(ApiGenExecute apiGenExecute)
    {
        return apiGenExecuteMapper.selectApiGenExecuteList(apiGenExecute);
    }

    /**
     * 新增api执行
     * 
     * @param apiGenExecute api执行
     * @return 结果
     */
    @Override
    public int insertApiGenExecute(ApiGenExecute apiGenExecute)
    {
        apiGenExecute.setCreateTime(DateUtils.getNowDate());
        apiGenExecute.setSqlStatement(SqlStatementEscapeReductionUtils.escapeReduction(apiGenExecute.getSqlStatement()));
        log.info("apiGenExecute="+apiGenExecute.toString());
        String action = apiGenExecute.getAction();
        // 如果是CREATE或UPDATE 设置默认的出参json
        if(ApiGenConstants.CREATE.equals(action.trim().toLowerCase(Locale.ROOT))|| ApiGenConstants.UPDATE.equals(action.trim().toLowerCase(Locale.ROOT))){
            ApiGenAccess apiGenAccess = new ApiGenAccess();
            apiGenAccess.setApiId(apiGenExecute.getApiId());
            apiGenAccess.setUpdateTime(DateUtils.getNowDate());
            List<ApiGenDictData> apiGenDictCache = iApiGenService.getApiGenDictCache(action);
            apiGenAccess.setExoticGinseng(apiGenDictCache.get(0).getDictValue());
            apiGenAccessMapper.updateByApiIdApiGenAccess(apiGenAccess);
        }
        int i = apiGenExecuteMapper.insertApiGenExecute(apiGenExecute);
        iApiGenService.getApiGenExecuteCache(apiGenExecute.getId().toString());
        return i;
    }

    /**
     * 修改api执行
     * 
     * @param apiGenExecute api执行
     * @return 结果
     */
    @Override
    public int updateApiGenExecute(ApiGenExecute apiGenExecute)
    {
        apiGenExecute.setUpdateTime(DateUtils.getNowDate());
        apiGenExecute.setSqlStatement(SqlStatementEscapeReductionUtils.escapeReduction(apiGenExecute.getSqlStatement()));
        log.info("apiGenExecute="+apiGenExecute.toString());
        int i = apiGenExecuteMapper.updateApiGenExecute(apiGenExecute);
        ApiGenExecute apiGenExecute1 = apiGenExecuteMapper.selectApiGenExecuteById(apiGenExecute.getId());
        ApiGenCacheUtils.setApiGenExecuteCache(apiGenExecute1.getId().toString(),apiGenExecute1);
        return i;
    }

    /**
     * 批量删除api执行
     * 
     * @param ids 需要删除的api执行主键
     * @return 结果
     */
    @Override
    public int deleteApiGenExecuteByIds(Long[] ids)
    {
        int i = apiGenExecuteMapper.deleteApiGenExecuteByIds(ids);
        for (int k = 0; k < ids.length; k++) {
            ApiGenCacheUtils.removeApiGenExecuteCache(ids[k].toString());
        }
        return i;
    }

    /**
     * 删除api执行信息
     * 
     * @param id api执行主键
     * @return 结果
     */
    @Override
    public int deleteApiGenExecuteById(Long id)
    {
        int i = apiGenExecuteMapper.deleteApiGenExecuteById(id);
        iApiGenService.removeApiGenExecuteCache(id.toString());
        return i;
    }
}
