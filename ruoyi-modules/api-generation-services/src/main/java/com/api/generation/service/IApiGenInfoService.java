package com.api.generation.service;

import java.util.List;
import com.api.generation.domain.ApiGenInfo;

/**
 * api服务信息Service接口
 * 
 * @author gaochao
 * @date 2023-07-02
 */
public interface IApiGenInfoService 
{
    /**
     * 查询api服务信息
     * 
     * @param id api服务信息主键
     * @return api服务信息
     */
    public ApiGenInfo selectApiGenInfoById(Long id);

    /**
     * 查询api服务信息列表
     * 
     * @param apiGenInfo api服务信息
     * @return api服务信息集合
     */
    public List<ApiGenInfo> selectApiGenInfoList(ApiGenInfo apiGenInfo);

    /**
     * 新增api服务信息
     * 
     * @param apiGenInfo api服务信息
     * @return 结果
     */
    public Long insertApiGenInfo(ApiGenInfo apiGenInfo);

    /**
     * 修改api服务信息
     * 
     * @param apiGenInfo api服务信息
     * @return 结果
     */
    public int updateApiGenInfo(ApiGenInfo apiGenInfo);

    /**
     * 批量删除api服务信息
     * 
     * @param ids 需要删除的api服务信息主键集合
     * @return 结果
     */
    public int deleteApiGenInfoByIds(Long[] ids);

    /**
     * 删除api服务信息信息
     * 
     * @param id api服务信息主键
     * @return 结果
     */
    public int deleteApiGenInfoById(Long id);

    public ApiGenInfo selectApiGenInfoByApiPath(String servletPath);
}
