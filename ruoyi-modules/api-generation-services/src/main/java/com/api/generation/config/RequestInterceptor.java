package com.api.generation.config;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lombok.extern.log4j.Log4j2;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

@Log4j2
public class RequestInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        // 在处理请求之前进行操作，例如验证、日志记录等
        // 获取请求path路径
        String servletPath = request.getServletPath();
        // 保留原来的请求路径
        request.setAttribute("servletPath",servletPath);
        // 处理请求之前的自定义逻辑
        log.info(servletPath+"请求被拦截，进行处理...");
        // 构造转发的路径
        String forwardPath = "/execute/api";  // 替换成你想要转发的目标路径

        // 执行转发
        request.getRequestDispatcher(forwardPath).forward(request, response);

        // 返回 false 表示中断后续的请求处理
        return false; // 继续处理后续请求
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        // 在处理完请求并准备渲染视图之前进行操作，例如修改响应内容、记录日志等
        // 处理完请求之后的自定义逻辑
        log.info("请求处理完成。");
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        // 在视图渲染完成之后进行操作
        
        // 可以进行一些清理工作或其他善后操作
    }
}
