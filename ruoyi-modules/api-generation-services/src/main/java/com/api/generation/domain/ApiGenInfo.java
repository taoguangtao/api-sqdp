package com.api.generation.domain;

import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * api服务信息对象 api_gen_info
 *
 * @author gaochao
 * @date 2023-07-23
 */
public class ApiGenInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 操作类型 */
    @Excel(name = "操作类型")
    private String apiAction;

    /** api请求路径 */
    @Excel(name = "api请求路径")
    private String apiPath;


    @Excel(name = "api请求路径页面显示用")
    private String apiPathShow;

    /** api名称 */
    @Excel(name = "api名称")
    private String apiName;

    /** api请求类型 */
    @Excel(name = "api请求类型")
    private String apiType;

    /** api说明 */
    @Excel(name = "api说明")
    private String apiIllustrate;

    /** api分组 */
    @Excel(name = "api分组")
    private String apiGroup;

    /** api版本 */
    @Excel(name = "api版本")
    private String apiVersion;

    /** token（0 启用 1停用） */
    @Excel(name = "请求头", readConverterExp = "0=启用,1=停用")
    private String apiHeader;

    @Excel(name = "是否发布", readConverterExp = "0=已发布,1=未发布")
    private String apiPublish;

    /** 状态（0正常 1停用） */
    @Excel(name = "状态", readConverterExp = "0=正常,1=停用")
    private String status;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setApiAction(String apiAction)
    {
        this.apiAction = apiAction;
    }

    public String getApiAction()
    {
        return apiAction;
    }
    public void setApiPath(String apiPath)
    {
        this.apiPath = apiPath;
    }

    public String getApiPath()
    {
        return apiPath;
    }
    public void setApiPathShow(String apiPathShow)
    {
        this.apiPathShow = apiPathShow;
    }

    public String getApiPathShow()
    {
        return apiPathShow;
    }
    public void setApiName(String apiName)
    {
        this.apiName = apiName;
    }

    public String getApiName()
    {
        return apiName;
    }
    public void setApiType(String apiType)
    {
        this.apiType = apiType;
    }

    public String getApiType()
    {
        return apiType;
    }
    public void setApiIllustrate(String apiIllustrate)
    {
        this.apiIllustrate = apiIllustrate;
    }

    public String getApiIllustrate()
    {
        return apiIllustrate;
    }
    public void setApiGroup(String apiGroup)
    {
        this.apiGroup = apiGroup;
    }

    public String getApiGroup()
    {
        return apiGroup;
    }
    public void setApiVersion(String apiVersion)
    {
        this.apiVersion = apiVersion;
    }

    public String getApiVersion()
    {
        return apiVersion;
    }
    public void setApiToken(String apiHeader)
    {
        this.apiHeader = apiHeader;
    }

    public String getApiHeader()
    {
        return apiHeader;
    }
    public void setApiPublish(String apiPublish)
    {
        this.apiPublish = apiPublish;
    }

    public String getApiPublish()
    {
        return apiPublish;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getStatus()
    {
        return status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("apiAction", getApiAction())
                .append("apiPath", getApiPath())
                .append("apiPath", getApiPathShow())
                .append("apiName", getApiName())
                .append("apiType", getApiType())
                .append("apiIllustrate", getApiIllustrate())
                .append("apiGroup", getApiGroup())
                .append("apiVersion", getApiVersion())
                .append("apiHeader", getApiHeader())
                .append("apiPublish", getApiPublish())
                .append("status", getStatus())
                .append("createBy", getCreateBy())
                .append("createTime", getCreateTime())
                .append("updateBy", getUpdateBy())
                .append("updateTime", getUpdateTime())
                .append("remark", getRemark())
                .toString();
    }
}
