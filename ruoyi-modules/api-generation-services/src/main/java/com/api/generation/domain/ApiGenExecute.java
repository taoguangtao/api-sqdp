package com.api.generation.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * api执行对象 api_gen_execute
 * 
 * @author gaochao
 * @date 2023-07-02
 */
public class ApiGenExecute extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** api的id */
    @Excel(name = "api的id")
    private Long apiId;

    /** 数据库类型 */
    @Excel(name = "数据库类型")
    private String databaseType;

    /** 数据库名 */
    @Excel(name = "数据库名")
    private String databaseName;

    /** 表名 */
    @Excel(name = "表名")
    private String tableName;

    /** 操作类型 CREATE、UPDATE、SELECT、EXPORT */
    @Excel(name = "操作类型 CREATE、UPDATE、SELECT、EXPORT")
    private String action;

    /** 条件json格式类型 */
    @Excel(name = "条件json格式类型")
    private String conditional;

    /** 更新字段json格式类型 */
    @Excel(name = "更新字段json格式类型")
    private String updateField;

    /** sql语句，当操作类型为SELECT时才会有值 */
    @Excel(name = "sql语句，当操作类型为SELECT时才会有值")
    private String sqlStatement;

    /** 状态（0正常 1停用） */
    @Excel(name = "状态", readConverterExp = "0=正常,1=停用")
    private String status;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setApiId(Long apiId) 
    {
        this.apiId = apiId;
    }

    public Long getApiId() 
    {
        return apiId;
    }
    public void setDatabaseType(String databaseType) 
    {
        this.databaseType = databaseType;
    }

    public String getDatabaseType() 
    {
        return databaseType;
    }
    public void setDatabaseName(String databaseName) 
    {
        this.databaseName = databaseName;
    }

    public String getDatabaseName() 
    {
        return databaseName;
    }
    public void setTableName(String tableName) 
    {
        this.tableName = tableName;
    }

    public String getTableName() 
    {
        return tableName;
    }
    public void setAction(String action) 
    {
        this.action = action;
    }

    public String getAction() 
    {
        return action;
    }
    public void setConditional(String conditional) 
    {
        this.conditional = conditional;
    }

    public String getConditional() 
    {
        return conditional;
    }
    public void setUpdateField(String updateField) 
    {
        this.updateField = updateField;
    }

    public String getUpdateField() 
    {
        return updateField;
    }
    public void setSqlStatement(String sqlStatement) 
    {
        this.sqlStatement = sqlStatement;
    }

    public String getSqlStatement() 
    {
        return sqlStatement;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("apiId", getApiId())
            .append("databaseType", getDatabaseType())
            .append("databaseName", getDatabaseName())
            .append("tableName", getTableName())
            .append("action", getAction())
            .append("conditional", getConditional())
            .append("updateField", getUpdateField())
            .append("sqlStatement", getSqlStatement())
            .append("status", getStatus())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
