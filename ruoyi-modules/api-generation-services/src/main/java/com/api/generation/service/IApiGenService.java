package com.api.generation.service;

import cn.hutool.json.JSONObject;
import com.api.generation.domain.ApiGenAccess;
import com.api.generation.domain.ApiGenDictData;
import com.api.generation.domain.ApiGenExecute;
import com.api.generation.domain.ApiGenInfo;
import com.ruoyi.common.core.web.domain.AjaxResult;

import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.util.List;

/**
 * 数据服务通用接口Service接口
 * 
 * @author gaochao
 * @date 2023-07-02
 */
public interface IApiGenService
{

    void loadingApiGenInfoCache();

    void loadingApiGenExecuteCache();

    void loadingApiGenAccessCache();

    void loadingApiGenDictDataCache();

    ApiGenInfo getApiGenInfoCache(String apiPath);

    ApiGenExecute getApiGenExecuteCache(String id);

    ApiGenAccess getApiGenAccessCache(String id);

    List<ApiGenDictData> getApiGenDictCache(String id);

    void removeApiGenInfoCache(String apiPath);

    void removeApiGenExecuteCache(String id);

    void removeApiGenAccessCache(String id);

    void removeApiGenDictCache(String dictType);

    void clearApiGenInfoCache();

    void clearApiGenAccessCache();

    void clearApiGenExecuteCache();

    void clearApiGenDictCache();

    void resetApiGenInfoCache();

    void resetApiGenExecuteCache();

    void resetApiGenAccessCache();

    void resetApiGenDictCache();

    AjaxResult createImpl(JSONObject parameterFromBody, ApiGenInfo apiGenInfoCache, ApiGenAccess apiGenAccessCache, ApiGenExecute apiGenExecuteCache);

    AjaxResult updateImpl(JSONObject parameterFromBody, ApiGenInfo apiGenInfoCache, ApiGenAccess apiGenAccessCache, ApiGenExecute apiGenExecuteCache);

    AjaxResult selectImpl(JSONObject parameterFromBody, ApiGenInfo apiGenInfoCache, ApiGenAccess apiGenAccessCache, ApiGenExecute apiGenExecuteCache);

    AjaxResult exportImpl(JSONObject parameterFromBody, ApiGenInfo apiGenInfoCache, ApiGenAccess apiGenAccessCache, ApiGenExecute apiGenExecuteCache, HttpServletResponse response);

    AjaxResult getDBType(String dbtype);

    AjaxResult getDBTypeDBname(String dbtype, String dbname);

    AjaxResult getDBTypeDBnameTablename(String dbtype, String dbname, String tablename);

    DataSource debugDataSource(String dbtype);
}
