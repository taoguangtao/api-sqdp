package com.api.generation.controller;

import com.api.generation.config.GenerationConfig;
import com.api.generation.domain.ApiGenInfo;
import com.api.generation.service.IApiGenInfoService;
import com.ruoyi.common.core.constant.HttpStatus;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * api服务信息Controller
 * 
 * @author gaochao
 * @date 2023-07-02
 */
@RestController
@RequestMapping("/info")
public class ApiGenInfoController extends BaseController
{
    @Autowired
    private IApiGenInfoService apiGenInfoService;

    @Autowired
    private GenerationConfig generationConfig;

    /**
     * API服务请求前缀
     */
    @GetMapping("/apipathprefix")
    public AjaxResult getApiPathPrefix()
    {
        Object apipathprefix = generationConfig.getApipathprefix();
        return AjaxResult.success(apipathprefix);
    }

    /**
     * 查询api服务信息列表
     */
    @RequiresPermissions("generation:info:list")
    @GetMapping("/list")
    public TableDataInfo list(ApiGenInfo apiGenInfo)
    {
        startPage();
        List<ApiGenInfo> list = apiGenInfoService.selectApiGenInfoList(apiGenInfo);
        return getDataTable(list);
    }

    /**
     * 导出api服务信息列表
     */
    @RequiresPermissions("generation:info:export")
    @Log(title = "api服务信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ApiGenInfo apiGenInfo)
    {
        List<ApiGenInfo> list = apiGenInfoService.selectApiGenInfoList(apiGenInfo);
        ExcelUtil<ApiGenInfo> util = new ExcelUtil<ApiGenInfo>(ApiGenInfo.class);
        util.exportExcel(response, list, "api服务信息数据");
    }

    /**
     * 获取api服务信息详细信息
     */
    @RequiresPermissions("generation:info:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(apiGenInfoService.selectApiGenInfoById(id));
    }

    /**
     * 新增api服务信息
     */
    @RequiresPermissions("generation:info:add")
    @Log(title = "api服务信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ApiGenInfo apiGenInfo)
    {
        Long rows = apiGenInfoService.insertApiGenInfo(apiGenInfo);
        return rows > 0 ? AjaxResult.success(rows) : AjaxResult.error();
    }

    /**
     * 修改api服务信息
     */
    @RequiresPermissions("generation:info:edit")
    @Log(title = "api服务信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ApiGenInfo apiGenInfo)
    {
        return toAjax(apiGenInfoService.updateApiGenInfo(apiGenInfo));
    }

    /**
     * 删除api服务信息,关于本条api信息全部级联删除
     */
    @RequiresPermissions("generation:info:remove")
    @Log(title = "api服务信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(apiGenInfoService.deleteApiGenInfoByIds(ids));
    }
}
