package com.api.generation.mapper;

import java.util.List;
import com.api.generation.domain.ApiGenDictData;
import org.apache.ibatis.annotations.Param;

/**
 * api字典数据Mapper接口
 * 
 * @author gaochao
 * @date 2023-07-02
 */
public interface ApiGenDictDataMapper 
{
    /**
     * 查询api字典数据
     * 
     * @param dictCode api字典数据主键
     * @return api字典数据
     */
    public ApiGenDictData selectApiGenDictDataByDictCode(Long dictCode);

    /**
     * 查询api字典数据列表
     * 
     * @param apiGenDictData api字典数据
     * @return api字典数据集合
     */
    public List<ApiGenDictData> selectApiGenDictDataList(ApiGenDictData apiGenDictData);

    /**
     * 新增api字典数据
     * 
     * @param apiGenDictData api字典数据
     * @return 结果
     */
    public int insertApiGenDictData(ApiGenDictData apiGenDictData);

    /**
     * 修改api字典数据
     * 
     * @param apiGenDictData api字典数据
     * @return 结果
     */
    public int updateApiGenDictData(ApiGenDictData apiGenDictData);

    /**
     * 删除api字典数据
     * 
     * @param dictCode api字典数据主键
     * @return 结果
     */
    public int deleteApiGenDictDataByDictCode(Long dictCode);

    /**
     * 批量删除api字典数据
     * 
     * @param dictCodes 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteApiGenDictDataByDictCodes(Long[] dictCodes);

    List<ApiGenDictData> selectDictDataByType(String dictType);

    public int countDictDataByType(String dictType);

    public int updateDictDataType(@Param("oldDictType") String oldDictType, @Param("newDictType") String newDictType);

}
