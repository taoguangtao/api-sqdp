package com.api.generation.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.api.generation.domain.ApiGenAccess;
import com.api.generation.service.IApiGenAccessService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * api出入参Controller
 * 
 * @author gaochao
 * @date 2023-07-15
 */
@RestController
@RequestMapping("/access")
public class ApiGenAccessController extends BaseController
{
    @Autowired
    private IApiGenAccessService apiGenAccessService;

    /**
     * 查询api出入参列表
     */
    @GetMapping("/list")
    public TableDataInfo list(ApiGenAccess apiGenAccess)
    {
        startPage();
        List<ApiGenAccess> list = apiGenAccessService.selectApiGenAccessList(apiGenAccess);
        return getDataTable(list);
    }

    /**
     * 导出api出入参列表
     */
    @Log(title = "api出入参", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ApiGenAccess apiGenAccess)
    {
        List<ApiGenAccess> list = apiGenAccessService.selectApiGenAccessList(apiGenAccess);
        ExcelUtil<ApiGenAccess> util = new ExcelUtil<ApiGenAccess>(ApiGenAccess.class);
        util.exportExcel(response, list, "api出入参数据");
    }

    /**
     * 获取api出入参详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(apiGenAccessService.selectApiGenAccessById(id));
    }

    @GetMapping(value = "/byapi/{apiId}")
    public AjaxResult getInfoByApi(@PathVariable("apiId") Long apiId)
    {
        return success(apiGenAccessService.selectApiGenAccessByApiId(apiId));
    }

    /**
     * 新增api出入参
     */
    @Log(title = "api出入参", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ApiGenAccess apiGenAccess)
    {
        return toAjax(apiGenAccessService.insertApiGenAccess(apiGenAccess));
    }

    /**
     * 修改api出入参
     */
    @Log(title = "api出入参", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ApiGenAccess apiGenAccess)
    {
        return toAjax(apiGenAccessService.updateApiGenAccess(apiGenAccess));
    }

    @Log(title = "根据apiId修改api出入参", businessType = BusinessType.UPDATE)
    @PutMapping("/byapiid")
    public AjaxResult updateAccessByApiId(@RequestBody ApiGenAccess apiGenAccess)
    {
        return toAjax(apiGenAccessService.updateAccessByApiId(apiGenAccess));
    }

    /**
     * 删除api出入参
     */
    @Log(title = "api出入参", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(apiGenAccessService.deleteApiGenAccessByIds(ids));
    }
}
