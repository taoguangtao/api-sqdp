package com.api.generation.mapper;

/**
 * 数据服务通用接口Mapper接口
 * 
 * @author gaochao
 * @date 2023-07-02
 */
public interface ApiGenMapper
{

    public int insertApiGen(String sql);

    public int updateApiGen(String sql);
}
