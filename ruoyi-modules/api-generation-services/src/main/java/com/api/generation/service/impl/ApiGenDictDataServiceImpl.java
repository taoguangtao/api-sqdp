package com.api.generation.service.impl;

import com.api.generation.domain.ApiGenDictData;
import com.api.generation.mapper.ApiGenDictDataMapper;
import com.api.generation.service.IApiGenDictDataService;
import com.api.generation.service.IApiGenService;
import com.api.generation.util.ApiGenCacheUtils;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.common.core.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * api字典数据Service业务层处理
 *
 * @author gaochao
 * @date 2023-07-02
 */
@Service
public class ApiGenDictDataServiceImpl implements IApiGenDictDataService
{
    @Autowired
    private ApiGenDictDataMapper apiGenDictDataMapper;
    @Autowired
    private IApiGenService iApiGenService;
    /**
     * 查询api字典数据
     *
     * @param dictCode api字典数据主键
     * @return api字典数据
     */
    @Override
    public ApiGenDictData selectApiGenDictDataByDictCode(Long dictCode)
    {
        return apiGenDictDataMapper.selectApiGenDictDataByDictCode(dictCode);
    }

    /**
     * 查询api字典数据列表
     *
     * @param apiGenDictData api字典数据
     * @return api字典数据
     */
    @Override
    public List<ApiGenDictData> selectApiGenDictDataList(ApiGenDictData apiGenDictData)
    {
        return apiGenDictDataMapper.selectApiGenDictDataList(apiGenDictData);
    }

    /**
     * 新增api字典数据
     *
     * @param apiGenDictData api字典数据
     * @return 结果
     */
    @Override
    public int insertApiGenDictData(ApiGenDictData apiGenDictData)
    {
        apiGenDictData.setCreateTime(DateUtils.getNowDate());
        int i = apiGenDictDataMapper.insertApiGenDictData(apiGenDictData);
        iApiGenService.resetApiGenDictCache();
        return i;
    }

    /**
     * 修改api字典数据
     *
     * @param apiGenDictData api字典数据
     * @return 结果
     */
    @Override
    public int updateApiGenDictData(ApiGenDictData apiGenDictData)
    {
        apiGenDictData.setUpdateTime(DateUtils.getNowDate());
        int i = apiGenDictDataMapper.updateApiGenDictData(apiGenDictData);
        iApiGenService.resetApiGenDictCache();
        return i;
    }

    /**
     * 批量删除api字典数据
     *
     * @param dictCodes 需要删除的api字典数据主键
     * @return 结果
     */
    @Override
    public int deleteApiGenDictDataByDictCodes(Long[] dictCodes)
    {
        int i = apiGenDictDataMapper.deleteApiGenDictDataByDictCodes(dictCodes);
        iApiGenService.resetApiGenDictCache();
        return i;
    }

    /**
     * 删除api字典数据信息
     *
     * @param dictCode api字典数据主键
     * @return 结果
     */
    @Override
    public int deleteApiGenDictDataByDictCode(Long dictCode)
    {
        int i = apiGenDictDataMapper.deleteApiGenDictDataByDictCode(dictCode);
        iApiGenService.resetApiGenDictCache();
        return i;
    }

    @Override
    public List<ApiGenDictData> selectDictDataByType(String dictType) {
        List<ApiGenDictData> dictDatas = iApiGenService.getApiGenDictCache(dictType);
        if (StringUtils.isNotEmpty(dictDatas))
        {
            return dictDatas;
        }
        dictDatas = apiGenDictDataMapper.selectDictDataByType(dictType);
        if (StringUtils.isNotEmpty(dictDatas))
        {
            ApiGenCacheUtils.setApiGenDictCache(dictType, dictDatas);
            return dictDatas;
        }
        return null;
    }
}
