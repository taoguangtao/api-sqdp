package com.api.generation.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "generation")
public class GenerationConfig {

    private String apipathprefix;

    private String dicttypearray;


    public boolean getDicttypearrayArray(String targetString ){
        String replace = dicttypearray.replace("{", "").replace("}", "").replace("'", "");
        String[] array = replace.split(",");
        // 判断字符串是否存在于数组中
        boolean exists = false;
        for (String s : array) {
            if (s.equals(targetString)) {
                exists = true;
                break;
            }
        }
        return exists;
    }

    public void setDicttypearray(String dicttypearray) {
        this.dicttypearray = dicttypearray;
    }

    public String getDicttypearray() {
        return dicttypearray;
    }

    public String getApipathprefix() {
        return apipathprefix;
    }

    public void setApipathprefix(String apipathprefix) {
        this.apipathprefix = apipathprefix;
    }
}
