package com.api.generation.controller;

import com.api.generation.config.GenerationConfig;
import com.api.generation.domain.ApiGenDictType;
import com.api.generation.service.IApiGenDictTypeService;
import com.api.generation.service.IApiGenService;
import com.ruoyi.common.core.utils.StringUtils;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.common.security.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 数据字典信息
 * @author chaogao
 * @date 2023/8/24 22:43
 */
@RestController
@RequestMapping("/dict/type")
public class ApiGenDictTypeController extends BaseController
{
    @Autowired
    private IApiGenDictTypeService dictTypeService;

    @Autowired
    private IApiGenService iApiGenService;

    @Autowired
    private GenerationConfig generationConfig;

    @RequiresPermissions("generation:dict:list")
    @GetMapping("/list")
    public TableDataInfo list(ApiGenDictType dictType)
    {
        startPage();
        List<ApiGenDictType> list = dictTypeService.selectDictTypeList(dictType);
        return getDataTable(list);
    }

    @Log(title = "字典类型", businessType = BusinessType.EXPORT)
    @RequiresPermissions("generation:dict:export")
    @PostMapping("/export")
    public void export(HttpServletResponse response, ApiGenDictType dictType)
    {
        List<ApiGenDictType> list = dictTypeService.selectDictTypeList(dictType);
        ExcelUtil<ApiGenDictType> util = new ExcelUtil<ApiGenDictType>(ApiGenDictType.class);
        util.exportExcel(response, list, "字典类型");
    }

    /**
     * 查询字典类型详细
     */
    @RequiresPermissions("generation:dict:query")
    @GetMapping(value = "/{dictId}")
    public AjaxResult getInfo(@PathVariable Long dictId)
    {
        return success(dictTypeService.selectDictTypeById(dictId));
    }

    /**
     * 新增字典类型
     */
    @RequiresPermissions("generation:dict:add")
    @Log(title = "字典类型", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@Validated @RequestBody ApiGenDictType dict)
    {
        if (!dictTypeService.checkDictTypeUnique(dict))
        {
            return error("新增字典'" + dict.getDictName() + "'失败，字典类型已存在");
        }
        dict.setCreateBy(SecurityUtils.getUsername());
        return toAjax(dictTypeService.insertDictType(dict));
    }

    /**
     * 修改字典类型
     */
    @RequiresPermissions("generation:dict:edit")
    @Log(title = "字典类型", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@Validated @RequestBody ApiGenDictType dict)
    {
        // 对系统自带的字典类型进行保护不能进行修改
        if(StringUtils.isNotEmpty(dict.getDictType())){
            if(generationConfig.getDicttypearrayArray(dict.getDictType())){
                return error("系统内置字典类型不能更改'" + dict.getDictType());
            }
        }
        if (!dictTypeService.checkDictTypeUnique(dict))
        {
            return error("修改字典'" + dict.getDictName() + "'失败，字典类型已存在");
        }
        dict.setUpdateBy(SecurityUtils.getUsername());
        return toAjax(dictTypeService.updateDictType(dict));
    }

    /**
     * 删除字典类型
     */
    @RequiresPermissions("generation:dict:remove")
    @Log(title = "字典类型", businessType = BusinessType.DELETE)
    @DeleteMapping("/{dictIds}")
    public AjaxResult remove(@PathVariable Long[] dictIds)
    {
        dictTypeService.deleteDictTypeByIds(dictIds);
        return success();
    }

    /**
     * 刷新字典缓存
     */
    @RequiresPermissions("generation:dict:remove")
    @Log(title = "字典类型", businessType = BusinessType.CLEAN)
    @DeleteMapping("/refreshCache")
    public AjaxResult refreshCache()
    {
        iApiGenService.resetApiGenDictCache();
        return success();
    }

    /**
     * 获取字典选择框列表
     */
    @GetMapping("/optionselect")
    public AjaxResult optionselect()
    {
        List<ApiGenDictType> dictTypes = dictTypeService.selectDictTypeAll();
        return success(dictTypes);
    }
}
