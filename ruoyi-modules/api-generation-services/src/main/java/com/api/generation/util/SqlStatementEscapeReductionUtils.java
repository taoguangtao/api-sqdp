package com.api.generation.util;

import cn.hutool.core.util.StrUtil;

/**
 * @Author chaogao
 * @Date 2023 09 13 17 41
 **/
public class SqlStatementEscapeReductionUtils {

    public static String escapeReduction(String sqlStatement) {
        if (StrUtil.isEmpty(sqlStatement)) {
            return null;
        }
        return sqlStatement.replaceAll("&gt;", ">").replaceAll("&lt;","<");
    }
}
