package com.api.generation.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class InterceptorConfig implements WebMvcConfigurer {

    @Autowired
    private GenerationConfig generationConfig;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        String apipathprefix = generationConfig.getApipathprefix();
        registry.addInterceptor(new RequestInterceptor())
                .addPathPatterns(apipathprefix+"**"); // 拦截所有请求
    }
}
