/*
package com.api.generation.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.stereotype.Component;

import javax.annotation.PreDestroy;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

*/
/**
 * 多数据源适配
 *//*

public class DatabaseConnector {
    private DataSource dataSource;
    private ExecutorService executorService;
    private static DatabaseConnector instanceMysql;
    private static DatabaseConnector instanceClickhouse;

    public DatabaseConnector(String databaseType, String host, int port, String database, String username, String password) {
        configureDataSource(databaseType, host, port, database, username, password);
        configureExecutorService();
    }

    public static synchronized DatabaseConnector getInstanceMysql(String databaseType, String host, int port, String database, String username, String password) {
        if (instanceMysql == null) {
            instanceMysql = new DatabaseConnector(databaseType, host, port, database, username, password);
        }
        return instanceMysql;
    }

    public static synchronized DatabaseConnector getInstanceClickhouse(String databaseType, String host, int port, String database, String username, String password) {
        if (instanceClickhouse == null) {
            instanceClickhouse = new DatabaseConnector(databaseType, host, port, database, username, password);
        }
        return instanceClickhouse;
    }

    private void configureDataSource(String databaseType, String host, int port, String database, String username, String password) {
        HikariConfig config = new HikariConfig();
        String url;

        if (databaseType.equals("mysql")) {
            url = "jdbc:mysql://" + host + ":" + port + "/" + database;
        } else if (databaseType.equals("clickhouse")) {
            url = "jdbc:clickhouse://" + host + ":" + port + "/" + database;
        } else {
            throw new IllegalArgumentException("Unsupported database type: " + databaseType);
        }

        config.setJdbcUrl(url);
        config.setUsername(username);
        config.setPassword(password);
        dataSource = new HikariDataSource(config);
    }

    private void configureExecutorService() {
        int threadCount = Runtime.getRuntime().availableProcessors(); // 可根据需求调整线程数
        executorService = Executors.newFixedThreadPool(threadCount);
    }

    public ResultSet executeQuery(String query) throws SQLException {
        Connection connection = dataSource.getConnection();
        Statement statement = connection.createStatement();
        return statement.executeQuery(query);
    }

    public void executeUpdate(String query) {
        executorService.execute(() -> {
            try (Connection connection = dataSource.getConnection();
                 Statement statement = connection.createStatement()) {
                statement.executeUpdate(query);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    @PreDestroy
    public void shutdown() {
        executorService.shutdown();
        try {
            if (!executorService.awaitTermination(10, TimeUnit.SECONDS)) {
                executorService.shutdownNow();
                if (!executorService.awaitTermination(10, TimeUnit.SECONDS)) {
                    System.err.println("Executor service did not terminate");
                }
            }
        } catch (InterruptedException e) {
            executorService.shutdownNow();
            Thread.currentThread().interrupt();
        }

        if (dataSource instanceof HikariDataSource) {
            ((HikariDataSource) dataSource).close();
        }
    }
}
*/
