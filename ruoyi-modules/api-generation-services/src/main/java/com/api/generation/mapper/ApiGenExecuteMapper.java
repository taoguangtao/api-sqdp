package com.api.generation.mapper;

import java.util.List;
import com.api.generation.domain.ApiGenExecute;

/**
 * api执行Mapper接口
 * 
 * @author gaochao
 * @date 2023-07-02
 */
public interface ApiGenExecuteMapper 
{
    /**
     * 查询api执行
     * 
     * @param id api执行主键
     * @return api执行
     */
    public ApiGenExecute selectApiGenExecuteById(Long id);

    /**
     * 查询api执行列表
     * 
     * @param apiGenExecute api执行
     * @return api执行集合
     */
    public List<ApiGenExecute> selectApiGenExecuteList(ApiGenExecute apiGenExecute);

    /**
     * 新增api执行
     * 
     * @param apiGenExecute api执行
     * @return 结果
     */
    public int insertApiGenExecute(ApiGenExecute apiGenExecute);

    /**
     * 修改api执行
     * 
     * @param apiGenExecute api执行
     * @return 结果
     */
    public int updateApiGenExecute(ApiGenExecute apiGenExecute);

    /**
     * 删除api执行
     * 
     * @param id api执行主键
     * @return 结果
     */
    public int deleteApiGenExecuteById(Long id);

    /**
     * 批量删除api执行
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteApiGenExecuteByIds(Long[] ids);

    ApiGenExecute selectApiGenExecuteByApiId(Long apiId);

    public void deleteApiGenExecuteByApiId(Long id);
}
