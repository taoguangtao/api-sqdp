package com.ruoyi.common.core.web.domain;

import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import com.ruoyi.common.core.constant.HttpStatus;
import com.ruoyi.common.core.utils.StringUtils;

/**
 * 操作消息提醒
 * 
 * @author ruoyi
 */
public class AjaxResult extends HashMap<String, Object>
{
    private static final long serialVersionUID = 1L;

    /** 总记录数 */
    private static final String TOTAL="total";

    /** 状态码 */
    public static final String CODE_TAG = "code";

    /** 返回内容 */
    public static final String MSG_TAG = "msg";

    /** 数据对象 */
    public static final String DATA_TAG = "data";

    /** 列表数据 */
    public static final String ROWS = "rows";

    /**
     * 初始化一个新创建的 AjaxResult 对象，使其表示一个空消息。
     */
    public AjaxResult()
    {
    }

    /**
     * 初始化一个新创建的 AjaxResult 对象
     * 
     * @param code 状态码
     * @param msg 返回内容
     */
    public AjaxResult(int code, String msg)
    {
        super.put(CODE_TAG, code);
        super.put(MSG_TAG, msg);
    }

    /**
     * 初始化一个新创建的 AjaxResult 对象
     * 
     * @param code 状态码
     * @param msg 返回内容
     * @param data 数据对象
     */
    public AjaxResult(int code, String msg, Object data)
    {
        super.put(CODE_TAG, code);
        super.put(MSG_TAG, msg);
        if (StringUtils.isNotNull(data))
        {
            super.put(DATA_TAG, data);
        }
    }

    public AjaxResult(int code, String msg, List<?> list,long total)
    {
        super.put(CODE_TAG, code);
        super.put(MSG_TAG, msg);
        super.put(TOTAL, total);
        super.put(ROWS, list);
    }

    /**
     * 返回成功消息
     * 
     * @return 成功消息
     */
    public static AjaxResult success()
    {
        return AjaxResult.success("操作成功");
    }

    /**
     * 返回成功数据
     * 
     * @return 成功消息
     */
    public static AjaxResult success(Object data)
    {
        return AjaxResult.success("操作成功", data);
    }

    public static AjaxResult success(List<?> list,long total)
    {
        return AjaxResult.success("操作成功", list,total);
    }

    /**
     * 返回成功消息
     * 
     * @param msg 返回内容
     * @return 成功消息
     */
    public static AjaxResult success(String msg)
    {
        return AjaxResult.success(msg, null);
    }

    /**
     * 返回成功消息
     * 
     * @param msg 返回内容
     * @param data 数据对象
     * @return 成功消息
     */
    public static AjaxResult success(String msg, Object data)
    {
        return new AjaxResult(HttpStatus.SUCCESS, msg, data);
    }

    public static AjaxResult success(String msg, List<?> list,long total)
    {
        return new AjaxResult(HttpStatus.SUCCESS, msg, list,total);
    }
    /**
     * 返回警告消息
     *
     * @param msg 返回内容
     * @return 警告消息
     */
    public static AjaxResult warn(String msg)
    {
        return AjaxResult.warn(msg, null);
    }

    /**
     * 返回警告消息
     *
     * @param msg 返回内容
     * @param data 数据对象
     * @return 警告消息
     */
    public static AjaxResult warn(String msg, Object data)
    {
        return new AjaxResult(HttpStatus.WARN, msg, data);
    }


    /**
     * 返回资源，服务未找到消息
     *
     * @param msg 返回内容
     * @return 资源，服务未找到消息
     */
    public static AjaxResult notfound(String msg)
    {
        return AjaxResult.notfound(msg, null);
    }

    /**
     * 返回资源，服务未找到消息
     *
     * @param msg 返回内容
     * @param data 数据对象
     * @return 资源，服务未找到消息
     */
    public static AjaxResult notfound(String msg, Object data)
    {
        return new AjaxResult(HttpStatus.NOT_FOUND, msg, data);
    }

    /**
     * 返回参数列表错误（缺少，格式不匹配）消息
     *
     * @param msg 返回内容
     * @return 参数列表错误（缺少，格式不匹配）
     */
    public static AjaxResult badrequest(String msg)
    {
        return AjaxResult.badrequest(msg, null);
    }

    /**
     * 返回参数列表错误（缺少，格式不匹配）消息
     *
     * @param msg 返回内容
     * @param data 数据对象
     * @return 参数列表错误（缺少，格式不匹配）
     */
    public static AjaxResult badrequest(String msg, Object data)
    {
        return new AjaxResult(HttpStatus.BAD_REQUEST, msg, data);
    }

    /**
     * 返回不支持的数据，媒体类型消息
     *
     * @param msg 返回内容
     * @return 不支持的数据，媒体类型
     */
    public static AjaxResult unsupportedtype(String msg)
    {
        return AjaxResult.unsupportedtype(msg, null);
    }

    /**
     * 返回不支持的数据，媒体类型消息
     *
     * @param msg 返回内容
     * @param data 数据对象
     * @return 不支持的数据，媒体类型
     */
    public static AjaxResult unsupportedtype(String msg, Object data)
    {
        return new AjaxResult(HttpStatus.UNSUPPORTED_TYPE, msg, data);
    }

    /**
     * 返回错误消息
     * 
     * @return 错误消息
     */
    public static AjaxResult error()
    {
        return AjaxResult.error("操作失败");
    }

    /**
     * 返回错误消息
     * 
     * @param msg 返回内容
     * @return 错误消息
     */
    public static AjaxResult error(String msg)
    {
        return AjaxResult.error(msg, null);
    }

    /**
     * 返回错误消息
     * 
     * @param msg 返回内容
     * @param data 数据对象
     * @return 错误消息
     */
    public static AjaxResult error(String msg, Object data)
    {
        return new AjaxResult(HttpStatus.ERROR, msg, data);
    }

    /**
     * 返回错误消息
     * 
     * @param code 状态码
     * @param msg 返回内容
     * @return 错误消息
     */
    public static AjaxResult error(int code, String msg)
    {
        return new AjaxResult(code, msg, null);
    }

    /**
     * 是否为成功消息
     *
     * @return 结果
     */
    public boolean isSuccess()
    {
        return Objects.equals(HttpStatus.SUCCESS, this.get(CODE_TAG));
    }

    /**
     * 是否为错误消息
     *
     * @return 结果
     */
    public boolean isError()
    {
        return !isSuccess();
    }

    /**
     * 方便链式调用
     *
     * @param key
     * @param value
     * @return
     */
    @Override
    public AjaxResult put(String key, Object value)
    {
        super.put(key, value);
        return this;
    }
}
