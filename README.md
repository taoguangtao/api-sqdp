
<h1 align="center" style="margin: 30px 0 30px; font-weight: bold;">API服务平台</h1>
<h4 align="center">基于SpringBoot开发的API服务平台</h4>

## 平台简介

一直在寻找一款可定制化API的工具，但发现市场上并没有完全符合要求的开源项目。因此，就有了这个系统， API服务平台是基于优秀的开源框架 [RuoYi](https://gitee.com/y_project/RuoYi) 进行二次开发。

## 平台功能

1. 保留原若依全部功能并再此基础上扩展了API服务平台。
2. 适用于各种Web应用程序，可以和微服务架构的应用无缝集成。
3. 提供全面的API管理功能。你可以轻松地添加、编辑和删除API。这让API的管理变得非常便捷。
4. 引入了高度自定义的查询功能。你可以根据具体需求定义和执行复杂的查询操作，这将大大减轻后端开发者的工作负担。
5. 你可以根据需要自定义导出数据，用于生成报告和数据分析。
6. 支持多种数据库，包括<b>MySQL</b>、<b>ClickHouse</b>和国产数据库<b>OpenGauss</b>扩展方便。

## 技术选型

*  系统环境 Java EE 8、Servlet 3.0、Apache Maven 3
*  主框架 Spring Boot 2.3.x、Spring Cloud Hoxton.SR9、Spring Framework 5.2.x、Spring Security 5.2.x
*  持久层 Apache MyBatis 3.5.x、Hibernate Validation 6.0.x、Alibaba Druid 1.2.x
*  视图层 Vue 2.6.x、Axios 0.21.0、Element 2.14.x
*  数据库 MySQL、ClickHouse、OpenGauss

## 系统模块

~~~
com.ruoyi     
├── ruoyi-ui              // 前端框架 [80]
├── ruoyi-gateway         // 网关模块 [8080]
├── ruoyi-auth            // 认证中心 [9200]
├── ruoyi-api             // 接口模块
│       └── ruoyi-api-system                          // 系统接口
├── ruoyi-common          // 通用模块
│       └── ruoyi-common-core                         // 核心模块
│       └── ruoyi-common-datascope                    // 权限范围
│       └── ruoyi-common-datasource                   // 多数据源
│       └── ruoyi-common-log                          // 日志记录
│       └── ruoyi-common-redis                        // 缓存服务
│       └── ruoyi-common-seata                        // 分布式事务
│       └── ruoyi-common-security                     // 安全模块
│       └── ruoyi-common-swagger                      // 系统接口
├── ruoyi-modules         // 业务模块
│       └── ruoyi-system                              // 系统模块 [9201]
│       └── ruoyi-gen                                 // 代码生成 [9202]
│       └── ruoyi-job                                 // 定时任务 [9203]
│       └── api-generation-services                   // API服务 [9205]
│       └── ruoyi-file                                // 文件服务 [9300]
├── ruoyi-visual          // 图形化管理模块 
│       └── ruoyi-visual-monitor                      // 监控中心 [9100]
├──pom.xml                // 公共依赖
~~~


## 若依原内置功能
*  用户管理：用户是系统操作者，该功能主要完成系统用户配置。
*  部门管理：配置系统组织机构（公司、部门、小组），树结构展现支持数据权限。
*  岗位管理：配置系统用户所属担任职务。
*  菜单管理：配置系统菜单，操作权限，按钮权限标识等。
*  角色管理：角色菜单权限分配、设置角色按机构进行数据范围权限划分。
*  字典管理：对系统中经常使用的一些较为固定的数据进行维护。
*  参数管理：对系统动态配置常用参数。
*  通知公告：系统通知公告信息发布维护。
*  操作日志：系统正常操作日志记录和查询；系统异常信息日志记录和查询。
*  登录日志：系统登录日志记录查询包含登录异常。
*  在线用户：当前系统中活跃用户状态监控。
*  定时任务：在线（添加、修改、删除)任务调度包含执行结果日志。
*  代码生成：前后端代码的生成（java、html、xml、sql）支持CRUD下载 。
*  系统接口：根据业务代码自动生成相关的api接口文档。
*  服务监控：监视当前系统CPU、内存、磁盘、堆栈等相关信息。
*  在线构建器：拖动表单元素生成相应的HTML代码。
*  连接池监视：监视当前系统数据库连接池状态，可进行分析SQL找出系统性能瓶颈。

## API服务平台单独部署带页面
#### 准备工作
* JDK >= 1.8 (推荐1.8版本)
* Mysql >= 5.7.0 (推荐5.7版本)
* Redis >= 3.0
* Maven >= 3.0
* Node >= 12
* nacos >= 2.0.4

#### 执行sql脚本
* 进入api-sqdp/sql下
* 导入数据脚本api-generation.sql（必须）
* 导入数据脚本ry-cloud.sql（必须）
* 导入数据脚本ry_config.sql（必须）
* 导入数据脚本quartz.sql（可选）
* 导入数据脚本ry_seata.sql（可选）

#### 运行基础模块（启动没有先后顺序）
* RuoYiGatewayApplication （网关模块 必须）
* RuoYiAuthApplication （认证模块 必须）
* RuoYiSystemApplication （系统模块 必须）
* ApiGenerationServicesApplication（api服务 必须）
* RuoYiMonitorApplication （监控中心 可选）
* RuoYiGenApplication （代码生成 可选）
* RuoYiJobApplication （定时任务 可选）
* RuoYFileApplication （文件服务 可选）

#### 前端运行
* 进入项目目录  cd ruoyi-ui
* 安装依赖 npm install
* 本地开发 启动项目 npm run dev

#### 访问API服务平台
打开浏览器，输入：(http://localhost:80 (opens new window)) 默认账户/密码 admin/admin123）

## 集成到现有微服务架构

#### 在已有环境mysql数据库中执行sql脚本
* 进入api-sqdp/sql下
* 导入数据脚本api-generation.sql（必须） 
* 导入通过api服务平台页面配置的api_gen_info、api_gen_access、api_gen_execute表中数据
* 完善api_gen_dict_data表中数据源信息

#### 导入api服务平台nacos配置
* 进入api-sqdp/nacos
* 导入api-generation-services-dev.yml

#### 现有网关新增配置
* 在网关中新增路由规则路由到api-generation-services服务  
* 修改nacos配置中的generation.apipathprefix的值(与现有网关路由前缀一致) 

#### 部署api服务平台
* ApiGenerationServicesApplication（api服务 必须）

#### 使用api服务
* 在原有的系统中就可以直接通过网关访问api服务平台中的接口了

#### api服务平台可以多节点部署，支持高并发场景

## 体验账号

系统按日常开发使用内置的账号 管理员：<b>admin/admin123</b>,后端开发人员：<b>server/server123456</b>， 前端开发人员：<b>web/web123456</b>， 测试：<b>test/test123456</b>。

## 演示图
<table>
    <tr>
        <td><img src="https://gitee.com/gaoc_383/api-sqdp/raw/master/ruoyi-ui/src/assets/images/screenshot/img_11.png"/></td>
        <td><img src="https://gitee.com/gaoc_383/api-sqdp/raw/master/ruoyi-ui/src/assets/images/screenshot/img.png"/></td>
    </tr>
    <tr>
        <td><img src="https://gitee.com/gaoc_383/api-sqdp/raw/master/ruoyi-ui/src/assets/images/screenshot/img_1.png"/></td>
        <td><img src="https://gitee.com/gaoc_383/api-sqdp/raw/master/ruoyi-ui/src/assets/images/screenshot/img_2.png"/></td>
    </tr>
    <tr>
        <td><img src="https://gitee.com/gaoc_383/api-sqdp/raw/master/ruoyi-ui/src/assets/images/screenshot/img_3.png"/></td>
        <td><img src="https://gitee.com/gaoc_383/api-sqdp/raw/master/ruoyi-ui/src/assets/images/screenshot/img_4.png"/></td>
    </tr>
    <tr>
        <td><img src="https://gitee.com/gaoc_383/api-sqdp/raw/master/ruoyi-ui/src/assets/images/screenshot/img_5.png"/></td>
        <td><img src="https://gitee.com/gaoc_383/api-sqdp/raw/master/ruoyi-ui/src/assets/images/screenshot/img_6.png"/></td>
    </tr>
    <tr>
        <td><img src="https://gitee.com/gaoc_383/api-sqdp/raw/master/ruoyi-ui/src/assets/images/screenshot/img_7.png"/></td>
        <td><img src="https://gitee.com/gaoc_383/api-sqdp/raw/master/ruoyi-ui/src/assets/images/screenshot/img_8.png"/></td>
    </tr>
    <tr>
        <td><img src="https://gitee.com/gaoc_383/api-sqdp/raw/master/ruoyi-ui/src/assets/images/screenshot/img_9.png"/></td>
        <td><img src="https://gitee.com/gaoc_383/api-sqdp/raw/master/ruoyi-ui/src/assets/images/screenshot/img_10.png"/></td>
    </tr>
</table>

